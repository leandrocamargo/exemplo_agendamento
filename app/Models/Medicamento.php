<?php

namespace App\Models;

use App\Helper\DataViewer;
use Illuminate\Database\Eloquent\Model;

class Medicamento extends Model
{
    use DataViewer;
    protected  $table    = 'medicamentos';
    protected  $fillable = [
        'principio_ativo',
        'laboratorio',
        'registro',
        'apresentacao',
        'tarja'
    ];
}
