<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConsultaAndamento extends Model
{
    protected $table    = 'consulta_andamento';
    protected $fillable = [
      'nome',
      'icone'
    ];

    public function consultas()
    {
      return $this->hasMany(Consulta::class,'andamento','id') ;
    }

}
