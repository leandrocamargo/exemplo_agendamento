<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resultado extends Model
{
    protected $table    = 'resultados';

    protected $fillable = [
      'valor'
    ];

    public function historico()
    {
        return $this->belongsTo(Historico::class);
    }

    public function resultadoExame()
    {
        return $this->belongsTo(Historico::class);
    }
}
