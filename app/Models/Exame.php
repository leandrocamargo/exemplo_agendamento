<?php

namespace App\Models;

use App\Helper\DataViewer;
use Illuminate\Database\Eloquent\Model;

class Exame extends Model
{
    use DataViewer;
    protected $table = 'exames';

    protected $fillable = [
        'nome',
        'cod'
    ];

    public function combos () {
      return $this->belongsToMany(ComboExame::class, 'pivot_combos_exames','exame_id','combo_exame_id');
    }

    public function caracteristicas () {
        return $this->hasMany(ExamesCaracteristicas::class,'exame_id','id');
    }
}
