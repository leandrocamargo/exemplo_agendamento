<?php

namespace App\Models;

use App\Helper\DataViewer;
use Illuminate\Database\Eloquent\Model;

class Convenio extends Model
{
  use DataViewer;
  protected $table    = 'convenios';
  protected $fillable =  [
    'nome'
  ];
    public function paciente()
    {
        return $this->hasMany(Paciente::class);
    }
}
