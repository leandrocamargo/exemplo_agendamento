<?php

namespace App\Models;

use App\Helper\DataViewer;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    use DataViewer;
    protected $table = 'pacientes';
    protected $fillable = [
       'matricula',
       'nome',
       'data_nascimento',
       'sexo',
       'estado_civil',
       'cor',
       'naturalidade',
       'grau_instrucao',
       'rg',
       'orgao_emissor',
       'cpf',
       'titular_cpf',
       'profissao',
       'previsao_retorno',
       'email',
       'indicacao',
       'cep',
       'logradouro',
       'complemento',
       'bairro',
       'cidade',
       'uf',
       'observacao',
       'foto',
       'user_id_created',
       'user_id_updated'
    ];

    public function convenio()
    {
      return $this->belongsTo(Convenio::class);
    }

    public function documentos()
    {
      return $this->hasMany(Documento::class);
    }

    public function consultas()
    {
      return $this->hasMany(Consulta::class)->orderBy('data_hora','desc');
    }

    public function historicos()
    {
      return $this->hasMany(Historico::class)->with('anotacoes')->where('doutor_id','=',auth()->id());
    }

    public function historicoAcompanhamentos()
    {
      return $this->hasManyThrough(Acompanhamento::class,Historico::class)->orderBy('data','desc');
    }

    public function historicoResultadosExames()
    {
      return $this->hasManyThrough(Resultado::class,Historico::class)->orderBy('id','desc');
    }
}
