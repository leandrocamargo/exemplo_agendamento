<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistoricoAnotacao extends Model
{
    protected $table    = 'historico_anotacoes';
    protected $fillable = [
        'data_anotacao',
        'anotacao',
        'historico_id'
    ];

    public function historico()
    {
        return $this->belongsTo(Historico::class,'historico_id');
    }
}
