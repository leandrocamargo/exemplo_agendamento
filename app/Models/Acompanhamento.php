<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Acompanhamento extends Model
{
    protected $table = 'acompanhamentos';
    protected $fillable = [
        'data',
        'id_gestacao',
        'peso',
        'pa',
        'au',
        'mf',
        'bcf',
        'edema',
        'historico_id'
    ];

    public function acompanhamento()
    {
        return $this->belongsTo(Historico::class,'historico_id');
    }
}
