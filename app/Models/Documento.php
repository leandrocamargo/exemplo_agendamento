<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    const ATESTEADO            = 'ATESTADO';
    const ANAMNESE             = 'ANAMNESE';
    const RECEITUARIO_SIMPLES  = 'RECEITUARIO SIMPLES';
    const RECEITUARIO_ESPECIAL = 'RECEITUARIO ESPECIAL';
    const LICENCA_MATERNIDADE  = 'LICENCA MATERNIDADE';


}
