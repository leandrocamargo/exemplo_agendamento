<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
    protected $table = 'horarios';
    protected $fillable = [
        'inicio_consulta',
        'fim_consulta',
        'horario_especial',
        'data_horario_especial'
    ];
}
