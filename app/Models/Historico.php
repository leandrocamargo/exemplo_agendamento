<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Historico extends Model
{
    protected $table    = 'paciente_historico';
    protected $fillable = [
        'paciente_id',
        'doutor_id'
    ];
    public function paciente()
    {
      return $this->belongsTo(Paciente::class);
    }

    public function combo()
    {
      return $this->belongsTo(ComboExame::class);
    }

    public function anotacoes()
    {
        return $this->hasMany(HistoricoAnotacao::class)->orderBy('data_anotacao','desc');
    }

    public function acompanhamentos()
    {
      return $this->hasMany(Acompanhamento::class);
    }

}
