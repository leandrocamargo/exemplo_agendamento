<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use PhpParser\Node\Expr\Cast\Double;

class Consulta extends Model
{
    protected $table    = 'consultas';
    protected $fillable = [
      'data_hora','doutor_id','paciente_id'
    ];

    public function status()
    {
      return $this->belongsTo(ConsultaStatus::class,'status');
    }

    public function andamento()
    {
      return $this->belongsTo(ConsultaAndamento::class,'andamento');
    }

    public function tipo() {
        return $this->belongsTo(TipoDeConsulta::class);
    }

    public function doutor()
    {
      return $this->belongsTo(User::class);
    }

    public function paciente()
    {
      return $this->belongsTo(Paciente::class)->with('historicos');
    }

    public function horario()
    {
      return $this->belongsTo(Horario::class);
    }
}
