<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    const APROVADA            = 'APROVADA';
    const AGUARDANDO_RESPOSTA = 'AGUARDANDO RESPOSTA';
    const NEGADA              = 'NEGADA';

    protected $fillable = [
        'cod',
        'url',
        'value',
        'deployment_date',
        'client_id',
        'status'
    ];

    public function client ()
    {
        return $this->belongsTo(Client::class,'client_id','id');
    }
}
