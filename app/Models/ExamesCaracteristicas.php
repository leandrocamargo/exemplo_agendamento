<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExamesCaracteristicas extends Model
{
    protected $table    = 'exames_caracteristicas';
    protected $fillable = [
       'nome',
       'exame_id'
    ];

    public function exame()
    {
      return $this->belongsTo(Exame::class,'exame_id');
    }
}
