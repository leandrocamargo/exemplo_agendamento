<?php

namespace App\Models;

use Artesaos\Defender\Traits\HasDefender;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable,HasDefender;

    const ADMIN  = 'ADMIN';
    const DOUTOR = 'DOUTOR';
    const USER   = 'USER';

    protected $fillable = [
        'name', 'email', 'password', 'crm'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getRoleAttribute()
    {
        return $this->roles()->first();
    }

    public function toArray ()
    {
        $role = [];
        if($this->role) {
            $role = [
                'id'   => $this->role->id,
                'name' => $this->role->name
            ];
        }
        return [
            'id'    => (int) $this->id,
            'name'  => $this->name,
            'email' => $this->email,
            'crm'   => $this->crm,
            'role'   => $role
        ];
    }
}
