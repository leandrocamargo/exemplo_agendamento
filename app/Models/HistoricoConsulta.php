<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricoConsulta extends Model
{
    protected $table = 'historicos_consulta';

    const RETORNO              = 'RETORNO';
    const CANCELADO            = 'CANCELADO';
    const NAO_COMPARECEU       = 'NÃO COMPARECEU';
    const REMARCADO_GRAVIDANZA = 'Remarcado Gravidanza';


}
