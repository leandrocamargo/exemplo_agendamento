<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConsultaStatus extends Model
{
    protected $table    = 'consulta_status';
    protected $fillable =  [
      'nome'
    ];

    public function consultas()
    {
      return $this->hasMany(Consulta::class,'andamento','id') ;
    }
}
