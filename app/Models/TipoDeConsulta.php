<?php

namespace App\Models;

use App\Helper\DataViewer;
use Illuminate\Database\Eloquent\Model;

class TipoDeConsulta extends Model
{
    use DataViewer;
    protected $table    = 'tipos_de_consulta';
    protected $fillable =  [
        'nome'
    ];
}
