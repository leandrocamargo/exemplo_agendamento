<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComboExame extends Model
{
  protected $table = 'combos_exames';
  protected $fillable = [
      'nome',
      'folhas'
  ];

  public function exames () {
    return $this->belongsToMany(Exame::class, 'pivot_combos_exames','combo_exame_id','exame_id')
        ->withPivot('folha','id')
        ->orderBy('pivot_combos_exames.folha');
  }

    public function getExamesCombo () {
        return $this->belongsToMany(Exame::class, 'pivot_combos_exames','combo_exame_id','exame_id');
    }
}
