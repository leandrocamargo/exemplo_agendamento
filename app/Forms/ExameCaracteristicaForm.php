<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 01/11/2018
 * Time: 10:59
 */

namespace App\Forms;


class ExameCaracteristicaForm extends BaseForm
{
    public function id()
    {
        return $this->request->id;
    }

    public function nome()
    {
        return $this->request->nome;
    }

    public function exame_id()
    {
        return $this->request->exame_id;
    }

}