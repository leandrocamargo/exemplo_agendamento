<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 27/09/2018
 * Time: 12:49
 */

namespace App\Forms;


class TiposDeConsultasForm extends BaseForm
{
    public function id()
    {
        return $this->request->id;
    }
    public function nome()
    {
        return $this->request->nome;
    }
}