<?php
namespace App\Forms;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
class BaseForm
{
    protected $request;

    public function __construct(FormRequest $request)
    {
        $this->request = $request;
    }
    protected function transformDate($value)
    {
        return Carbon::createFromFormat('d/m/Y', $value);
    }
}