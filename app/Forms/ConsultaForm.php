<?php

namespace App\Forms;

use App\Models\Horario;
use App\Models\TipoDeConsulta;
use App\Models\User;

class ConsultaForm extends BaseForm
{
    public function id()
    {
       return $this->request->id;
    }
    public function data_hora()
    {
      return $this->request->data_hora;
    }
    public function doutor_id()
    {
      return $this->request->get('doutor_id');
    }
    public function tipo_id()
    {
      $tipo = $this->request->get('tipo_id');
      return TipoDeConsulta::query()->findOrFail($tipo['id']);
    }
    public function paciente_id()
    {
      return $this->request->paciente_id;
    }
    public function status()
    {
      return $this->request->status_id;
    }
    public function andamento()
    {
      return $this->request->andamento_id;
    }
    public function nome()
    {
      return $this->request->nome;
    }
    public function horario_id()
    {
      $horario = $this->request->get('horario_id');
      return Horario::query()->findOrFail($horario['id']);
    }
    public function rg()
    {
      return $this->request->rg;
    }
}