<?php
/**
 * Created by PhpStorm.
 * User: Tmontec
 * Date: 03/09/2018
 * Time: 17:29
 */

namespace App\Forms;


use App\Models\Convenio;

class PacienteForm extends BaseForm
{
    public function data_hora()
    {
      return $this->request->data_hora;
    }

    public function matricula()
    {
      return $this->request->matricula;
    }

    public function nome()
    {
      return $this->request->nome;
    }

    public function data_nascimento()
    {
      return $this->request->data_nascimento;
    }

    public function sexo()
    {
      return $this->request->sexo;
    }

    public function estado_civil()
    {
      return $this->request->estado_civil;
    }

    public function cor()
    {
      return $this->request->cor;
    }

    public function convenio_id()
    {
        $convenio = $this->request->get('convenio_id');
        return Convenio::query()->findOrFail($convenio['id']);
    }

    public function naturalidade()
    {
      return $this->request->naturalidade;
    }

    public function grau_instrucao()
    {
      return $this->request->grau_instrucao;
    }

    public function rg()
    {
      return $this->request->rg;
    }

    public function orgao_emissor()
    {
      return $this->request->orgao_emissor;
    }

    public function cpf()
    {
      return $this->request->cpf;
    }

    public function titular_cpf()
    {
      return $this->request->titular_cpf;
    }

    public function profissao()
    {
      return $this->request->profissao;
    }

    public function previsao_retorno()
    {
      return $this->request->previsao_retorno;
    }

    public function email()
    {
      return $this->request->email;
    }

    public function indicacao()
    {
      return $this->request->indicacao;
    }

    public function cep()
    {
      return $this->request->cep;
    }

    public function logradouro()
    {
      return $this->request->logradouro;
    }

    public function complemento()
    {
      return $this->request->complemento;
    }

    public function bairro()
    {
      return $this->request->bairro;
    }

    public function cidade()
    {
      return $this->request->cidade;
    }

    public function uf()
    {
      return $this->request->uf;
    }

    public function observacao()
    {
      return $this->request->observacao;
    }

    public function foto()
    {
      return $this->request->foto;
    }

    public function user_id_created()
    {
        return $this->request->user_id_created;
    }

    public function user_id_updated()
    {
        return $this->request->user_id_updated;
    }
}
