<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 23/10/2018
 * Time: 13:11
 */

namespace App\Forms;


class AcompanhamentoForm extends BaseForm
{
    public function id()
    {
        return $this->request->id;
    }

    public function data()
    {
        return $this->request->data;
    }

    public function id_gestacao()
    {
        return $this->request->id_gestacao;
    }

    public function peso()
    {
        return $this->request->peso;
    }

    public function pa()
    {
        return $this->request->pa;
    }

    public function au()
    {
        return $this->request->au;
    }

    public function mf()
    {
        return $this->request->mf;
    }

    public function bcf()
    {
        return $this->request->bcf;
    }

    public function edema()
    {
        return $this->request->edema;
    }

    public function historico_id()
    {
        return $this->request->historico_id;
    }
}