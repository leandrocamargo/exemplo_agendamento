<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 27/11/2018
 * Time: 11:53
 */

namespace App\Forms;


class ResultadoForm extends BaseForm
{
    public function id()
    {
        return $this->request->id;
    }

    public function valor()
    {
        return $this->request->valor;
    }

    public function historico_id()
    {
        return $this->request->historico_id;
    }

    public function caracteristica_exame_id()
    {
        return $this->request->caracteristica_exame_id;
    }

}