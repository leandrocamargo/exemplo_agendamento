<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 08/10/2018
 * Time: 12:23
 */

namespace App\Forms;


use App\Models\Historico;
use App\Models\HistoricoAnotacao;

class HistoricoAnotacaoForm extends BaseForm
{
    public function id()
    {
        return $this->request->id;
    }

    public function data_anotacao()
    {
        return $this->request->data_anotacao;
    }
//
    public function anotacao()
    {
        return $this->request->anotacao;
    }
//
    public function historico_id()
    {
        return $this->request->historico_id;
    }
}