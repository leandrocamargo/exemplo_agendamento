<?php
/**
 * Created by PhpStorm.
 * User: Tmontec
 * Date: 03/09/2018
 * Time: 17:29
 */

namespace App\Forms;


class ConvenioForm extends BaseForm
{
    public function id()
    {
      return $this->request->id;
    }
    public function nome()
    {
      return $this->request->nome;
    }
}