<?php

namespace App\Forms;

use App\Models\Paciente;
use App\Models\User;

class HistoricoForm extends BaseForm
{
    public function id()
    {
      return $this->request->id;
    }
    public function doutor_id()
    {
      $doutor = $this->request->get('doutor_id');
      return User::query()->findOrFail($doutor['id']);
    }
    public function paciente_id()
    {
      $paciente = $this->request->get('paciente_id');
      return Paciente::query()->findOrFail($paciente['id']);
    }
}