<?php
Route::resource('convenios', 'Api\ConveniosController', ['except' => ['create', 'edit']]);
Route::get('convenio-search','Api\ConveniosController@search')->name('convenios.search');