<?php
//-------------------------------------------------
//GET
Route::get('/horario', 'Api\HorariosController@index')->name('horarios.index');
Route::get('/get-horario', 'Api\HorariosController@getHorarios')->name('horarios.get.horarios');
Route::get('/get-horarios-livres/{ids}', 'Api\HorariosController@getHorariosLivres')->name('horarios.get.horarios.livres');