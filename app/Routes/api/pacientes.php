<?php
Route::get('/paciente-get-historico/{pacienteId}','Api\PacientesController@getHistorico')->name('pacientes.get.historico');
Route::get('pacientes','Api\PacientesController@index')->name('paciente.index');
Route::post('pacientes-upload','Api\PacientesController@excelUpload')->name('pacientes.upload');
Route::get('pacientes-search','Api\PacientesController@search')->name('pacientes.search');
