<?php
Route::get('historico-anotacao/{id}','Api\HistoricoAnotacaoController@index')->name('historico-anotacao.index');
Route::get('historico-anotacao-delete/{id}','Api\HistoricoAnotacaoController@delete')->name('historico-anotacao.delete');
Route::post('historico-anotacao','Api\HistoricoAnotacaoController@store')->name('historico-anotacao.store');
Route::put('historico-anotacao/{id}','Api\HistoricoAnotacaoController@edit')->name('historico-anotacao.edit');
Route::put('historico-anotacao/{id}','Api\HistoricoAnotacaoController@update')->name('historico-anotacao.update');



