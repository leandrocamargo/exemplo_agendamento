<?php
Route::get('exame-caracteristica/{id}','Api\ExamesCaracteristicasController@index')->name('exame-caracteristica.index');
Route::get('exame-caracteristica-delete/{id}','Api\ExamesCaracteristicasController@delete')->name('exame-caracteristica.delete');
Route::post('exame-caracteristica-store','Api\ExamesCaracteristicasController@store')->name('exame-caracteristica.store');
Route::put('exame-caracteristica-update/{id}','Api\ExamesCaracteristicasController@update')->name('exame-caracteristica.update');

