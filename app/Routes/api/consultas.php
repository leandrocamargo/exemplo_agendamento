<?php
//-------------------------------------------------
//GET
Route::get('/consultas','Api\ConsultaController@index')->name('consulta.index');
Route::get('/consultas-by-doutor/{id}/{data}','Api\ConsultaController@consultasByDoctor')->name('consulta.by.doutor');
Route::get('/consulta-get-paciente/{rg}','Api\ConsultaController@consultasGetPaciente')->name('consulta.get.paciente');
Route::get('/consulta-change-status/{consulta}/{status}','Api\ConsultaController@consultasChangeStatus')->name('consulta.change.status');


//-------------------------------------------------
//POST
Route::post('/agendar-consulta','Api\ConsultaController@agendar')->name('consulta.agendar');