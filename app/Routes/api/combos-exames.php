<?php

//-------------------------------------------------
//GET
Route::get('combos-exames','Api\CombosExamesController@index')->name('combos-exames.index');
Route::get('combos-exames-pivot/{comboId}/{exameId}','Api\CombosExamesController@storeExame')->name('combos-exames.store.exame');
Route::get('combos-exames-change-seed/{pivotId}/{seed}','Api\CombosExamesController@changeSeed')->name('combos-exames.change.seed');
Route::get('combos-exames-delete/{comboId}/{exameId}','Api\CombosExamesController@delete')->name('combos-exames.delete');
Route::get('combos-exames-get-exames/{comboId}','Api\CombosExamesController@getExamesCombo')->name('combos-exames-get.exames');

//-------------------------------------------------
//POST
Route::post('combos-exames','Api\CombosExamesController@store')->name('combos-exames.store');