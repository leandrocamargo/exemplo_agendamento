<?php
Route::get('medicamentos','API\MedicamentosController@index')->name('medicamentos.index');

Route::post('medicamentos-upload','API\MedicamentosController@excelUpload')->name('medicamentos.upload');
Route::get('medicamentos-search','API\MedicamentosController@search')->name('medicamentos.search');