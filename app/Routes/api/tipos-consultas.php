<?php
Route::get('tipos-consultas','Api\TiposDeConsultasController@index')->name('tipos-consultas.index');
Route::resource('tipos-consultas','Api\TiposDeConsultasController',['except' =>['create','edit']]);
Route::get('tipos-consultas-search','Api\TiposDeConsultasController@search')->name('tipos-consultas.search');
Route::post('tipos-consultas-upload','Api\TiposDeConsultasController@excelUpload')->name('tipos-consultas.upload');