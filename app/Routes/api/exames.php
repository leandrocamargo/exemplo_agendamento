<?php
Route::get('exames','API\ExamesController@index')->name('exames.index');

Route::post('exames-upload','API\ExamesController@excelUpload')->name('exames.upload');
Route::get('exames-search','API\ExamesController@search')->name('exames.search');