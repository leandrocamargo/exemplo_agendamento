<?php
Route::get('resultado','API\ResultadosController@index')->name('resultado.index');
Route::post('resultado-store','API\ResultadosController@store')->name('resultado.store');
Route::put('resultado-update','API\ResultadosController@update')->name('resultado.update');