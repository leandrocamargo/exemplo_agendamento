<?php

namespace App\Service;

use App\Traits\CurrentUser;
use JeroenNoten\LaravelAdminLte\Menu\Builder;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;
use Defender;

class MyMenuFilter implements FilterInterface
{
    public function transform($item, Builder $builder)
    {
        $user = CurrentUser::get();

        if (isset($item['can']) &&  !Defender::canDo($item['can'])) {
            return false;
        }
//        if (isset($item['can']) &&  !Defender::roleExists("DOUTOR")) {
//            return false;
//        }

        return $item;
    }
}