<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 27/09/2018
 * Time: 12:53
 */

namespace App\Service;

use App\Forms\TiposDeConsultasForm;
use App\Http\Requests\TiposDeConsultasCreateRequest;
use App\Models\TipoDeConsulta;
use Illuminate\Support\Facades\DB;

class TiposDeConsultasCreateService
{
    public function create(TiposDeConsultasForm $form): TipoDeConsulta
    {
        return DB:: transaction(function () use ($form) {
            $data = [
                'nome' => $form->nome()
            ];
            $tiposConsultas = new TipoDeConsulta($data);
            $tiposConsultas->save();
            return $tiposConsultas;
        });
    }

}