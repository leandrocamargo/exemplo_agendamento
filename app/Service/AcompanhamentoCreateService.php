<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 23/10/2018
 * Time: 13:37
 */

namespace App\Service;


use App\Forms\AcompanhamentoForm;
use App\Models\Acompanhamento;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AcompanhamentoCreateService
{
    public function create(AcompanhamentoForm $form) : Acompanhamento
    {
        return DB::transaction(function () use ($form){
            $data = [
                'data'         => Carbon::now(),
                'id_gestacao'  => $form->id_gestacao(),
                'peso'         => $form->peso(),
                'pa'           => $form->pa(),
                'au'           => $form->au(),
                'mf'           => $form->mf(),
                'bcf'          => $form->bcf(),
                'edema'        => $form->edema(),
                'historico_id' => $form->historico_id()
            ];
            $acompanhamentoForm = new Acompanhamento($data);
            $acompanhamentoForm->save();
            return $acompanhamentoForm;
        });
    }

    public function update(AcompanhamentoForm $acompanhamentoForm)
    {
        return DB::transaction(function () use ($acompanhamentoForm){
            $acompanhamentoForm = Acompanhamento::findOfFail($acompanhamentoForm->id());
            $acompanhamentoForm->acompanhamento = $acompanhamentoForm->acompanhamento();
            $acompanhamentoForm->save();
        });
    }
}