<?php
/**
 * Created by PhpStorm.
 * User: Tmontec
 * Date: 21/07/2018
 * Time: 21:21
 */

namespace App\Service\Consulta;


use App\Events\Consulta\ConsultaCreated;
use App\Events\ConsultaStatusChanged;
use App\Models\ConsultaAndamento;
use App\Models\ConsultaStatus;
use App\Models\User;
use App\Service\PacienteCreateService;
use App\Forms\ConsultaForm;
use App\Models\Consulta;
use Illuminate\Support\Facades\DB;

class ConsultaCreateService
{
    private $service;
    function __construct(PacienteCreateService $service)
    {
        $this->service =  $service;
    }

    public function create(ConsultaForm $form): Consulta
    {
        return DB::transaction(function () use ($form) {
          $data = [
            'data_hora'    => $form->data_hora(),
            'paciente_id'  => $form->paciente_id(),
            'doutor_id'    => $form->doutor_id(),
            'andamento'    => $form->andamento(),
            'status'       => $form->status()
          ];
          $consulta = new Consulta($data);
          $consulta->horario()->associate($form->horario_id());
          $consulta->tipo()->associate($form->tipo_id());
          $consulta->save();
          event(new ConsultaCreated($consulta));
          return $consulta;
        });
    }

    public function update()
    {
        
    }

    public function checkAvailability (ConsultaForm $form): bool
    {
       $horario = $form->horario_id();
       $consulta = Consulta::query()
           ->where('data_hora', '=', $form->data_hora())
           ->where('doutor_id', '=', $form->doutor_id())
           ->where('horario_id','=', $horario->id)->get()->toArray();

       if (count($consulta) > 0) {
           return false;
       } else {
           return true;
       }
    }

    public function changeStatus ($consulta, $andamento)
    {
        $data = Consulta::findOrFail($consulta);
        $andamento = ConsultaAndamento::findOrFail($andamento);

        $data->andamento = $andamento->id;
        $user = User::find($data->doutor_id);
        event(new ConsultaStatusChanged($user ,$data));

        return $data->save();
    }
}