<?php
/**
 * Created by PhpStorm.
 * User: Tmontec
 * Date: 21/07/2018
 * Time: 21:21
 */

namespace App\Service;


use App\Models\Convenio;
use App\Forms\ConvenioForm;
use Illuminate\Support\Facades\DB;

class ConvenioCreateService
{
    public function create(ConvenioForm $form): Convenio
    {
        return DB:: transaction(function () use ($form) {
            $data = [
                'nome' => $form->nome()
            ];
            $convenio = new Convenio($data);
            $convenio->save();
            return $convenio;
        });
        
    }

    public function update(Convenio $form)
    {
        return DB::transaction(function () use ($form) {
            $convenio       = Convenio::findOfFail($form->id());
            $convenio->nome = $form->nome();
            $convenio->save();
            return $convenio;
        });
    }
}