<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 09/10/2018
 * Time: 10:41
 */

namespace App\Service;


use App\Forms\HistoricoAnotacaoForm;
use App\Models\HistoricoAnotacao;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class HistoricoAnotacaoCreateService
{
    public function create(HistoricoAnotacaoForm $form): HistoricoAnotacao
    {
        return DB::transaction(function () use ($form){
            $data = [
                'data_anotacao'  => Carbon::now(),
                'anotacao'       => $form->anotacao(),
                'historico_id'   => $form->historico_id()
            ];
            $histAnotForm = new HistoricoAnotacao($data);
            $histAnotForm->save();
            return $histAnotForm;
        });
    }

    public function update(HistoricoAnotacaoForm $historicoAnotacaoForm, $id)
    {
        return DB::transaction(function () use ($historicoAnotacaoForm, $id)
        {
            $histAnotForm                = HistoricoAnotacao::findOrFail($id);
            $histAnotForm->data_anotacao = $historicoAnotacaoForm->data_anotacao();
            $histAnotForm->anotacao      = $historicoAnotacaoForm->anotacao();
            $histAnotForm->historico_id  = $historicoAnotacaoForm->historico_id();
            $histAnotForm->save();
            return $histAnotForm;
        });
    }

    public function delete($id)
    {
        return DB::transaction(function () use ($id)
        {
            $histAnotForm             = HistoricoAnotacao::findOrFail($id);
            $histAnotForm->is_deleted = true;
            $histAnotForm->save();
            return $histAnotForm;
        });
    }
}