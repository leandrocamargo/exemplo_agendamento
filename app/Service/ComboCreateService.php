<?php
/**
 * Created by PhpStorm.
 * User: Tmontec
 * Date: 21/07/2018
 * Time: 21:21
 */

namespace App\Service;


use App\Forms\CombosExamesForm;
use App\Forms\ConsultaForm;
use App\Models\ComboExame;
use Illuminate\Support\Facades\DB;

class ComboCreateService
{
    public function create(CombosExamesForm $form): ComboExame
    {
       return DB::transaction(function () use ($form) {
         $data = [
           'nome' => $form->nome()
         ];
         $combo = new ComboExame($data);
         $combo->save();
         return $combo;
       });
    }

    public function update(CombosExamesForm $form): ComboExame
    {
        return DB::transaction(function () use ($form) {
            $combo       = ComboExame::findOfFail($form->id());
            $combo->nome = $form->nome();
            $combo->save();
            return $combo;
        });
    }
}