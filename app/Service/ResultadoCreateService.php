<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 27/11/2018
 * Time: 11:39
 */

namespace App\Service;


use App\Models\Resultado;
use Illuminate\Support\Facades\DB;

class ResultadoCreateService
{
    public function create(ResultadoForm $form) : Resultado
    {
        return DB::transaction(function () use ($form){
            $data = [
                'valor'                   => $form->valor(),
                'historico_id'            => $form->historico_id(),
                'caracteristica_exame_id' => $form->caracteristica_exame_id()
            ];
            return $data;
        });
    }
}