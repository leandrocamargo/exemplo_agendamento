<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 01/11/2018
 * Time: 10:16
 */

namespace App\Service;


use App\Forms\ExameCaracteristicaForm;
use App\Models\ExamesCaracteristicas;
use Illuminate\Support\Facades\DB;

class ExameCaracteristicaCreateService
{
    public function create(ExameCaracteristicaForm $form): ExamesCaracteristicas
    {
        return DB::transaction(function () use ($form){
            $data = [
                'nome'     => $form->nome(),
                'exame_id' => $form->exame_id(),
            ];
            $exameCaract = new ExamesCaracteristicas($data);
            $exameCaract->save();
            return $exameCaract;
        });
    }

    public function update(ExameCaracteristicaForm $exameCaracteristicaForm, $id)
    {
        return DB::transaction(function () use ($exameCaracteristicaForm, $id)
        {
           $exameCaract           = ExamesCaracteristicas::findOrFail($id);
           $exameCaract->exame_id = $exameCaracteristicaForm->exame_id();
           $exameCaract->nome     = $exameCaracteristicaForm->nome();
           $exameCaract->save();
           return $exameCaract;
        });
    }

    public function delete($id)
    {
        return DB::transaction(function () use ($id)
        {
            $exameCaract             = ExamesCaracteristicas::FindOrFail($id);
            $exameCaract->is_deleted = true;
            $exameCaract->save();
            return $exameCaract;
        });
    }
}