<?php
/**
 * Created by PhpStorm.
 * User: Tmontec
 * Date: 04/09/2018
 * Time: 14:55
 */

namespace App\Service;


use App\Events\PacienteCreated;
use App\Forms\ConsultaForm;
use App\Forms\PacienteForm;
use App\Models\Convenio;
use App\Models\Paciente;
use App\Traits\GerarMatriculaPaciente;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class PacienteCreateService
{
    public function create (PacienteForm $pacienteForm): Paciente
    {
        return DB::transaction(function () use ($pacienteForm) {
            $data = [
                'nome'         => $pacienteForm->nome(),
                'rg'           => $pacienteForm->rg(),
                'matricula'    => GerarMatriculaPaciente::gerarMatricula()

            ];
            $paciente = new Paciente($data);
            $paciente->convenio()->associate($pacienteForm->convenio_id());
            $paciente->save();
            event(new PacienteCreated($paciente));
            return $paciente;
        });
    }

    public static function createPaciente (FormRequest $form): Paciente
    {
        return DB::transaction(function () use ($form) {
            $data = [
                'nome'             => $form->get('nome'),
                'rg'               => $form->get('rg'),
                'matricula'        => GerarMatriculaPaciente::gerarMatricula(),
                'user_id_created'  => auth()->id(),
                'user_id_updated'  => auth()->id(),

            ];
            $paciente = new Paciente($data);
            $convenio = self::getConvenio($form);
            $paciente->convenio()->associate($convenio);
            $paciente->save();
            event(new PacienteCreated($paciente));
            return $paciente;
        });
    }

    private static function getConvenio (FormRequest $form) {
      $convenio  = $form->get('convenio_id');
      return Convenio::query()->findOrFail($convenio['id']);
    }
}