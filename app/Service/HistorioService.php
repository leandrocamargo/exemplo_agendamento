<?php
/**
 * Created by PhpStorm.
 * User: Tmontec
 * Date: 21/07/2018
 * Time: 21:21
 */

namespace App\Service;


use App\Models\Historico;
use App\Models\Paciente;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class HistorioService
{
    public function create(Paciente $paciente, User $doutor)
    {
        DB::transaction(function () use ($paciente, $doutor) {
            $data = [
                      'paciente_id' => $paciente->id,
                      'doutor_id'   => $doutor->id,
                      'create_at'   => Carbon::now(),
                      'update_at'   => Carbon::now()
                    ];
            $historico = new Historico($data);
            $historico->save();
        });
    }

    public function verifyHistorico (Paciente $paciente, User $doutor)
    {
      $historico = Historico::query()
                   ->where('paciente_id','=', $paciente->id)
                   ->where('doutor_id','=', $doutor->id)
                   ->get()
                   ->toArray();

      if (count($historico) > 0) {
       return false;
      } else {
       return true;
      }
    }
}