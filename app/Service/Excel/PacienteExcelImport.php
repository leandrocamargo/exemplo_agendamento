<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 04/10/2018
 * Time: 11:07
 */

namespace App\Services\Excel;


use App\Models\Paciente;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use SebastianBergmann\RecursionContext\Exception;

class PacienteExcelImport
{
    public function import($excel)
    {
        $file  = self::salvarArquivo($excel);
        $array = self::gerarExcel($file);
        return self::salvarPacientes($array);
    }

    private function salvarArquivo($arquivo)
    {
        $name = str_random(64);
        $extension = $arquivo->getClientOriginalExtension();
        Storage::putFileAs('public/excel/pacientes', $arquivo, "{$name}.{$extension}");
        $storedFile = public_path("storage/excel/pacientes/{$name}.{$extension}");
        try{
            $file = $storedFile;
            return $file;
        }catch (Exception $e) {
            echo 'Error' .$e->getMessage();
        }
    }

    private function gerarExcel($file)
    {
        try {
            return Excel::load($file , function ($reader) {
            })->toArray();
        } catch (Exception $e) {
            echo 'Error' .$e->getMessage();
        }
    }

    private function salvarPacientes($array)
    {
        foreach ($array as $pacientes){
            self::salvar($pacientes);
        }
        return true;
    }

    private function salvar($pacientes)
    {
        $data = [
            'nome' => $pacientes['Paci_tx_NomePaciente']
        ];

        $pacientes = new Paciente($data);
        $pacientes->save();
    }

}