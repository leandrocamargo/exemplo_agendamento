<?php
/**
 * Created by PhpStorm.
 * User: Tmontec
 * Date: 08/08/2018
 * Time: 17:04
 */

namespace App\Services\Excel;


use App\Models\Exame;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use SebastianBergmann\RecursionContext\Exception;

class ExamesExcelImport
{
    public function import($excel)
    {
        $file   = self::salvarArquivo($excel);
        $array  = self::gerarExcel($file);
        return self::salvarExames($array);
    }

    private function salvarArquivo ($arquivo)
    {
        $name = str_random(64);
        $extension = $arquivo->getClientOriginalExtension();
        Storage::putFileAs('public/excel/exames', $arquivo, "{$name}.{$extension}");
        $storedFile = public_path("storage/excel/exames/{$name}.{$extension}");
        try {
            $file = $storedFile;
            return $file;
        } catch (Exception $e) {
            echo 'Error '.$e->getMessage();
        }
    }

    private function gerarExcel ($file) {
        try {
            return Excel::load($file , function ($reader) {
            })->toArray();
        } catch (Exception $e) {
            echo 'Error '.$e->getMessage();
        }
    }

    private function salvar ($exame)
    {
        $data = [
            'cod'      => $exame['cod'],
            'nome'     => $exame['nome']
        ];

        if ($data['cod']) {
            $exame = new Exame($data);
            $exame->save();
        }
    }

    private function salvarExames ($array) {
        foreach ($array as $exames) {
            self::salvar($exames);
        }
        return true;
    }
}