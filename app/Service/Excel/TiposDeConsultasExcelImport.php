<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 02/10/2018
 * Time: 14:59
 */

namespace App\Services\Excel;


use App\Http\Requests\TiposDeConsultasCreateRequest;
use App\Models\TipoDeConsulta;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use SebastianBergmann\RecursionContext\Exception;

class TiposDeConsultasExcelImport
{
    public function import($excel)
    {
        $file   = self::salvarArquivo($excel);
        $array  = self::gerarExcel($file);
        return self::salvarTiposDeConsultas($array);
    }

    private function salvarArquivo($arquivo)
    {
        $name = str_random(64);
        $extension = $arquivo->getClientOriginalExtension();
        Storage::putFileAs('public/excel/tipos-consultas', $arquivo, "{$name}.{$extension}");
        $storedFile = public_path("storage/excel/tipos-consultas/{$name}.{$extension}");
        try {
            $file = $storedFile;
            return $file;
        } catch (Exception $e) {
            echo 'Error '.$e->getMessage();
        }
    }

    private function gerarExcel($file)
    {
        try {
            return Excel::load($file , function ($reader) {
            })->toArray();
        } catch (Exception $e) {
            echo 'Error '.$e->getMessage();
        }
    }

    private function salvar($tiposConsultas)
    {
        $data = [
           'nome' => $tiposConsultas['clas_tx_descricao']
        ];

        $tiposConsultas = new TipoDeConsulta($data);
        $tiposConsultas->save();

    }

    private function salvarTiposDeConsultas ($array) {
        foreach ($array as $tiposConsultas) {
            self::salvar($tiposConsultas);
        }
        return true;
    }

}