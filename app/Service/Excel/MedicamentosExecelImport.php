<?php
/**
 * Created by PhpStorm.
 * User: Tmontec
 * Date: 07/08/2018
 * Time: 09:27
 */

namespace App\Services\Excel;


use App\Models\Medicamento;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class MedicamentosExecelImport
{
  public function import($excel)
  {
      $file   = self::salvarArquivo($excel);
      $array  = self::gerarExcel($file);
      return self::salvarMedicamentos($array);
  }

  private function salvarArquivo ($arquivo)
  {
      $name = str_random(64);
      $extension = $arquivo->getClientOriginalExtension();
      Storage::putFileAs('public/excel/medicamentos', $arquivo, "{$name}.{$extension}");
      $storedFile = public_path("storage/excel/medicamentos/{$name}.{$extension}");
      try {
          $file = $storedFile;
          return $file;
      } catch (Exception $e) {
          echo 'Error '.$e->getMessage();
      }
  }

  private function gerarExcel ($file) {
      try {
          return Excel::load($file , function ($reader) {
                 })->toArray();
      } catch (Exception $e) {
          echo 'Error '.$e->getMessage();
      }
  }

  private function salvar ($medicamento)
  {
          $data = [
              'principio_ativo' => $medicamento['principio_ativo'],
              'laboratorio'     => $medicamento['laboratorio'],
              'registro'        => $medicamento['registro'],
              'apresentacao'    => $medicamento['apresentacao'],
              'tarja'           => $medicamento['tarja']
          ];

          $medicamento = new Medicamento($data);
          $medicamento->save();
  }

  private function salvarMedicamentos ($array) {
      foreach ($array as $medicamentos) {
          self::salvar($medicamentos);
      }
      return true;
  }
}