<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 04/10/2018
 * Time: 10:37
 */

namespace App\Http\Requests;


use App\Forms\PacienteForm;

class PacientesCreateRequest extends BaseRequest
{
    public function form(): PacienteForm
    {
        return new PacienteForm($this);
    }

    public function authoriza()
    {
        return true;
    }

    public function rules()
    {
        return[
            'nome' => 'required'
        ];
    }

    public function save()
    {

    }
}