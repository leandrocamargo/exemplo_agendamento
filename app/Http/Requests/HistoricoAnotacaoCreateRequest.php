<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 09/10/2018
 * Time: 10:29
 */

namespace App\Http\Requests;


use App\Forms\HistoricoAnotacaoForm;

class HistoricoAnotacaoCreateRequest extends BaseRequest
{
    public function form(): HistoricoAnotacaoForm
    {
        return new HistoricoAnotacaoForm($this);
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'anotacao' => 'required'
        ];
    }

    public function save()
    {
    }
}