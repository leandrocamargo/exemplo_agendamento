<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 28/09/2018
 * Time: 10:26
 */

namespace App\Http\Requests;

use App\Forms\TiposDeConsultasForm;
use App\Helper\DataViewer;
use Illuminate\Database\Eloquent\Model;

class TiposDeConsultasCreateRequest extends BaseRequest
{
    public function form(): TiposDeConsultasForm
    {
        return new TiposDeConsultasForm($this);
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required'
        ];
    }

    public function save()
    {
    }
}