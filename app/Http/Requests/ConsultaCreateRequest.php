<?php

namespace App\Http\Requests;


use App\Forms\CombosExamesForm;
use App\Forms\ConsultaForm;
use Illuminate\Foundation\Http\FormRequest;

class ConsultaCreateRequest extends BaseRequest
{
    public function form(): ConsultaForm
    {
      return new ConsultaForm($this);
    }

    public function authorize()
    {
      return true;
    }

    public function rules()
    {
       return [
           'data_hora'      => 'required',
           'paciente_id'    => 'sometimes',
           'convenio_id'    => 'required',
           'nome'           => 'required',
           'rg'             => 'required',
           'doutor_id'      => 'required',
           'horario_id'     => 'required'
       ];
    }

}
