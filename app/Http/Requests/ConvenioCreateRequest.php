<?php

namespace App\Http\Requests;

use App\Forms\ConvenioForm;
use Illuminate\Foundation\Http\FormRequest;

class ConvenioCreateRequest extends BaseRequest
{
    public function form(): ConvenioForm
    {
        return new ConvenioForm($this);
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required'
        ];
    }
}
