<?php

namespace App\Http\Requests;


use App\Forms\CombosExamesForm;
use Illuminate\Foundation\Http\FormRequest;

class CombosExamesCreateRequest extends BaseRequest
{
    public function form(): CombosExamesForm
    {
      return new CombosExamesForm($this);
    }

    public function authorize()
    {
      return true;
    }

    public function rules()
    {
       return [
          'nome' => 'required'
       ];
    }

}
