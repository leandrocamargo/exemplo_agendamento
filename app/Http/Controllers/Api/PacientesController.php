<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PacientesCreateRequest;
use App\Models\Paciente;
use App\Models\User;
use App\Service\PacienteCreateService;
use App\Services\Excel\PacienteExcelImport;
use Artesaos\Defender\Role;
use Illuminate\Http\Request;

class PacientesController extends Controller
{
    private $service;
    public function __construct(PacienteCreateService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $pacientes = Paciente::query()->get()->all();
        return $pacientes;
//        $doutores = [];
//        $regras = Role::query()->with('users')->where('name','=',User::DOUTOR)->get()->toArray();
//        $doutores = $regras[0]['users'];
//        return $doutores;
    }

    public function search()
    {
        $columns = [
            'nome' => ['name' => 'nome', 'label' => 'Nome', 'width' =>200],
        ];
        $model = Paciente::searchPaginateAndOrder($columns);
        return [
            'model'   => $model,
            'columns' => $columns,
            'action_width' => 108
        ];
    }

    public function excelUpload()
    {
        $files = request()->file('file');
        $excel = new PacienteExcelImport();
        foreach ($files as $file){
            if($excel->import($file)){
                $response = [
                    'message' => 'O arquivo foi enviado com sucesso',
                    'data'    => request()->file('file')
                ];
                return $response;
            }
        }
    }

    public function getHistorico ($pacienteId)
    {
        return $paciente = Paciente::query()
                            ->with('convenio')
                            ->with('documentos')
                            ->with('consultas')
                            ->with('historicos')
                            ->with('historicoAcompanhamentos')
                            ->with('historicoResultadosExames')
                            ->where('id','=',$pacienteId)
                            ->get()
                            ->toArray();
    }

    public function store(PacientesCreateRequest $request)
    {
        $data = $this->service->create($request->form());
        $response = [
            'message' => 'Paciente cadastrado com sucesso!!!',
            'data' => $data
        ];
        return $response;
    }
}
