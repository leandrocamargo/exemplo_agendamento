<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ConsultaStatus;

class StatusController extends Controller
{
    public function getStatus()
    {
        $status = ConsultaStatus::query()->get()->toArray();
        return $status;
    }
}
