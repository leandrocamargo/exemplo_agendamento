<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CombosExamesCreateRequest;
use App\Models\ComboExame;
use App\Models\Exame;
use App\Service\ComboCreateService;
use Illuminate\Support\Facades\DB;

class CombosExamesController extends Controller
{
    private $service;
    public function __construct(ComboCreateService $service)
    {
      $this->service = $service;
    }

    public function index()
    {
      $combos = ComboExame::query()
          ->with('exames')
          ->get()
          ->toArray();
      return $combos;
    }

    public function store(CombosExamesCreateRequest $request)
    {
       $data = $this->service->create($request->form());
       $response = [
           'message' => 'Combo adicionado com sucesso !!!',
           'data'    =>  $data
       ];
       return $response;
    }

    public function storeExame ($comboId, $exameId)
    {
        $combo = ComboExame::findOrFail($comboId);
        $exame = Exame::findOrFail($exameId);
        $combo->exames()->attach($exame->id);

        $combo = ComboExame::query()
            ->with('exames')
            ->where('id','=', $combo->id)
            ->get()
            ->toArray();

        $response = [
            'message' => 'Exame anexado com sucesso!!',
            'data'    =>  $combo
        ];
        return $response;
    }

    public function delete ($comboId, $exameId)
    {
        $combo = ComboExame::findOrFail($comboId);
        $exame = Exame::findOrFail($exameId);
        $combo->exames()->detach($exame->id);
        $response = [
            'message' => 'Exame excluido com sucesso!!',
            'data'    => $combo
        ];

        return $response;
    }

    public function getExamesCombo($id)
    {
        $combo = ComboExame::findOrFail($id)->with('getExamesCombo');
        return dd($combo->toArray());
    }

    public function changeSeed ($pivotId, $seed) {

        DB::table('pivot_combos_exames')
        ->where('id',$pivotId)
        ->update(['folha' => $seed]);

        return [
            'message' => 'Atualizado com Sucesso !!'
        ];
    }
}
