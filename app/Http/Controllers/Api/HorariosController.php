<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Consulta;
use App\Models\Historico;
use App\Models\Horario;
use Illuminate\Http\Request;

class HorariosController extends Controller
{
    public function index ()
    {
       $horario = Horario::query()->get()->all();
       return $horario;
    }

    public function getHorariosLivres ($ids)
    {
      $idsIn = explode(',',$ids);
      $horario = Horario::query()->whereNotIn('id',$idsIn)->get()->toArray();
      return $horario;
    }
}
