<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 08/10/2018
 * Time: 12:21
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\HistoricoAnotacaoCreateRequest;
use App\Models\HistoricoAnotacao;
use App\Service\HistoricoAnotacaoCreateService;

class HistoricoAnotacaoController extends Controller
{
    private $service;

    public function __construct(HistoricoAnotacaoCreateService $service)
    {
        $this->service = $service;
    }

    public function index($id)
    {
        $historicoAnotacao = HistoricoAnotacao::query()
            ->where('historico_id', '=', $id)
            ->where('is_deleted', 'is', false)
            ->get()
            ->all();
        return $historicoAnotacao;
    }

    public function store(HistoricoAnotacaoCreateRequest $request)
    {
        $data = $this->service->create($request->form());
        $response = [
            'message' => 'Historico cadastrado com sucesso!!!',
            'data' => $data
        ];
        return $response;
    }

    public function update(HistoricoAnotacaoCreateRequest $request, $id)
    {
        $data = $this->service->update($request->form(),$id);
        $response = [
            'message' => 'Historico editado com sucesso!!!',
            'data' => $data
        ];
        return $response;
    }

    public function delete($id)
    {
        $delete = $this->service->delete($id);
        $response = [
            'message' => 'Historico deletado com sucesso!!!',
            'data' => $delete
        ];
        return $response;
    }
}