<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ConsultaAndamento;
use Illuminate\Http\Request;

class AndamentosConsultaController extends Controller
{
    public function index()
    {
        $andamentos = ConsultaAndamento::query()
                       ->where('is_deleted','<>',1)
                       ->get()
                       ->toArray();
        return $andamentos;
    }
}
