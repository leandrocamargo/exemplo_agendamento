<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 27/11/2018
 * Time: 11:32
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\ResultadoCreateRequest;
use App\Models\Resultado;
use App\Service\ResultadoCreateService;

class ResultadosController extends Controller
{
    public function __construct(ResultadoCreateService $service)
    {
        $this->service = $service;
    }

    public function index($id)
    {
        $resultado = Resultado::query()
            ->where('historico_id','=',$id)
            ->get()
            ->all();

        return $resultado;
    }

    public function store(ResultadoCreateRequest $request, $id)
    {
        $data = $this->service->create($request->form(),$id);
        $response = [
            'message' => 'Resultado salvo com sucesso!!!',
            'data'    => $data
        ];
        return $response;
    }

}