<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ConvenioCreateRequest;
use App\Models\Convenio;
use App\Service\ConvenioCreateService;
use Illuminate\Http\Request;

class ConveniosController extends Controller
{
    private $service;
    public function __construct(ConvenioCreateService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $convenios = Convenio::query()
            ->where('is_deleted', '<>', 1)
            ->get()
            ->toArray();

        return $convenios;
    }

    public function store(ConvenioCreateRequest $request)
    {
        $data = $this->service->create($request->form());
        $response = [
            'message' => 'Convênio adicionado com sucesso !!!',
            'data'    =>  $data
        ];
        return $response;
    }

    public function search ()
    {
        $columns = [
            'id'   => ['name' => 'id', 'label' => 'ID', 'width' => 64],
            'nome'  => ['name' => 'nome', 'label' => 'Nome', 'width' => 200],

        ];
        $model = Convenio::searchPaginateAndOrder($columns);
        return [
            'model' => $model,
            'columns' => $columns,
            'action_width' => 108
        ];
    }
}
