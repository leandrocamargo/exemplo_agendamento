<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Exame;
use App\Services\Excel\ExamesExcelImport;

class ExamesController extends Controller
{
    public function index()
    {
      $exames = Exame::query()->get()->all();
      return $exames;
    }

    public function excelUpload ()
    {
        $files = request()->file('file');
        $excel = new ExamesExcelImport();
        foreach ($files as $file) {
            if($excel->import($file)) {
                $response = [
                    "message" => "O arquivo foi enviado com sucesso!!!",
                    "data"    => request()->file('file')
                ];
                return $response;
            }
        }
    }

    public function search()
    {
        $columns = [
            'id'   => ['name' => 'id', 'label' => 'ID', 'width' => 64],
            'cod'  => ['name' => 'cod', 'label' => 'Cod', 'width' => 80],
            'nome' => ['name' => 'nome', 'label' => 'Nome', 'width' => 250]
        ];
        $model = Exame::searchPaginateAndOrder($columns);
        return [
            'model' => $model,
            'columns' => $columns,
            'action_width' => 108
        ];
    }


}
