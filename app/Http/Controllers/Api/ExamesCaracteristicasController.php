<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 01/11/2018
 * Time: 10:13
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\ExamesCaracteristicasCreateRequest;
use App\Models\ExamesCaracteristicas;
use App\Service\ExameCaracteristicaCreateService;

class ExamesCaracteristicasController extends Controller
{
    private $service;

    public function __construct(ExameCaracteristicaCreateService $service)
    {
        $this->service = $service;
    }

    public function index($id)
    {
        $exameCaracteristica = ExamesCaracteristicas::query()
            ->where('exame_id',$id)
            ->where('is_deleted','is',false)
            ->get()
            ->all();
        return $exameCaracteristica;
    }

    public function store(ExamesCaracteristicasCreateRequest $request)
    {
        $data = $this->service->create($request->form());
        $response = [
            'message' => 'Característica inserida com sucesso!!!',
            'data' => $data
        ];
        return $response;
    }

    public function update(ExamesCaracteristicasCreateRequest $request, $id)
    {
        $data = $this->service->update($request->form(),$id);
        $response = [
            'message' => 'Caracteristica editada com sucesso!!!',
            'data' => $data
        ];
        return $response;
    }

    public function delete($id)
    {
        $delete = $this->service->delete($id);
        $response = [
            'message' => 'Característica deletada com sucesso!!!',
            'data' => $delete
        ];
        return $response;
    }
}