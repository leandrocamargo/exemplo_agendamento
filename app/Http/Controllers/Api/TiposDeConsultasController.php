<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TiposDeConsultasCreateRequest;
use App\Models\TipoDeConsulta;
use App\Service\TiposDeConsultasCreateService;
use App\Services\Excel\TiposDeConsultasExcelImport;
use Illuminate\Http\Request;

class TiposDeConsultasController extends Controller
{
    private $service;
    public function __construct(TiposDeConsultasCreateService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
       $tiposConsultas = TipoDeConsulta::query()->get()->all();
       return $tiposConsultas;
    }

    public function store(TiposDeConsultasCreateRequest $request)
    {
        $data = $this->service->create($request->form());
        $response = [
            'message' => 'Tipo de consulta cadastrado com sucesso!!!',
            'data'    =>  $data
        ];
        return $response;
    }

    public function excelUpload()
    {
        $files = request()->file('file');
        $excel = new TiposDeConsultasExcelImport();
        foreach ($files as $file){
            if($excel->import($file)){
                $response = [
                    "message" => "O arquivo foi enviado com sucesso!!!",
                    "data"    => request()->file('file')
                ];
                return $response;
            }
        }
    }

    public function search()
    {
        $columns = [
            //'id'   => ['name' => 'id', 'label' => 'ID', 'width' => 64],
            'nome' => ['name' => 'nome', 'label' => 'Nome', 'width' => 200],
        ];
        $model = TipoDeConsulta::searchPaginateAndOrder($columns);
        return [
           'model'   => $model,
           'columns' => $columns,
           'action_width' => 108

        ];
    }
}
