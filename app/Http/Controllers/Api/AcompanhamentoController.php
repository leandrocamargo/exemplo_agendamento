<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AcompanhamentoCreateRequest;
use App\Models\Acompanhamento;
use App\Service\AcompanhamentoCreateService;
use Illuminate\Http\Request;

class AcompanhamentoController extends Controller
{
    private $service;
    public function __construct(AcompanhamentoCreateService $service)
    {
        $this->service = $service;
    }

    public function index($id)
    {
        $acompanhamento = Acompanhamento::query()
            ->where('historico_id','=',$id)
            ->get()
            ->all();

        $index = 0;
        $array = [];

        foreach ($acompanhamento as $chave => $valor){
            $array ['peso'][$index] = $valor->peso;
            $array ['pa'][$index] = $valor->pa;
            $array ['id_gestacao'][$index] = $valor->id_gestacao;
            $array ['label'][$index] = $valor->data;
            $index++;
        }

        $response = [
            'message' => 'Acompanhamento gerado com sucesso!!!',
            'data'    =>  $array
        ];

        return $response;
    }

    public function store(AcompanhamentoCreateRequest $request)
    {
        $data = $this->service->create($request->form());
        $response=[
            'message' => 'Acompanhamento cadastrado com sucesso!!!',
            'data' => $data
        ];
        return $response;
    }
}
