<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Convenio;
use App\Models\Medicamento;
use App\Services\Excel\MedicamentosExecelImport;
use Illuminate\Http\Request;

class MedicamentosController extends Controller
{

    public function excelUpload ()
    {
        $files = request()->file('file');
        $excel = new MedicamentosExecelImport();
        foreach ($files as $file) {
            if($excel->import($file)) {
                $response = [
                    "message" => "O aquivo foi enviado com sucesso!!!",
                    "data"    => request()->file('file')
                ];
                return $response;
            }
        }
    }

    public function search()
    {
        $columns = [
            'id'              => ['name' => 'id', 'label' => 'ID', 'width' => 64],
            'principio_ativo' => ['name' => 'principio_ativo', 'label' => 'Principio Ativo', 'width' => 200],
            'apresentacao'    => ['name' => 'apresentacao', 'label' => 'Apresentacao', 'width' => 280]
        ];
        $model = Medicamento::searchPaginateAndOrder($columns);
        return [
            'model' => $model,
            'columns' => $columns,
            'action_width' => 108
        ];
    }
}
