<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ConsultaCreateRequest;
use App\Models\Consulta;
use App\Models\ConsultaAndamento;
use App\Models\ConsultaStatus;
use App\Models\Historico;
use App\Models\Paciente;
use App\Models\User;
use App\Service\Consulta\ConsultaCreateService;
use App\Service\PacienteCreateService;
use Artesaos\Defender\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ConsultaController extends Controller
{
    private $service;
    public function __construct(ConsultaCreateService $service)
    {
      $this->service = $service;
      $this->middleware('auth');
    }

    public function index()
    {
        $doutores = Role::query()
            ->with('users')
            ->where('name','=',User::DOUTOR)
            ->get()
            ->toArray();
        return $doutores;
    }

    public function consultasByDoctor ($id, $data)
    {
       $consultas = Consulta::query()
                    ->with('horario')
                    ->with('andamento')
                    ->with('status')
                    ->with('tipo')
                    ->with('paciente')
                    ->where('doutor_id','=', $id)
                    ->where('data_hora','LIKE','%'.$data.'%')
                    ->orderBy('data_hora')
                    ->get();
       return $consultas;
    }

    public function agendar(ConsultaCreateRequest $request)
    {

      $data = [];

        /**
         *
         * verificar a disponibilidade de horários
         */
      if ($this->service->checkAvailability($request->form())) {
          if ($request->paciente_id == 0) {
              $request->paciente_id = self::salvarPaciente($request);
              $data = $this->service->create($request->form());
          } else {
              $data = $this->service->create($request->form());
          }
          $response = [
              'message' => 'Consulta Salva com sucesso.',
              'data'    =>  $data,
              'error'   =>  false
          ];
      } else {
          $response = [
             'message' => 'O Horário para esta data já está agendado.',
             'data'    =>  $data,
             'error'   =>  true
          ];
      }
      return $response;
    }

    private function salvarPaciente ($request) {
        $paciente = PacienteCreateService::createPaciente($request);
        return $paciente->id;
    }

    public function consultasGetPaciente($rg)
    {
      $paciente = Paciente::query()
          ->where('rg','=',$rg)
          ->with('convenio')
          ->get()->toArray();
      return $paciente;
    }

    public function consultasChangeStatus ($consulta, $andamento)
    {
        $data = $this->service->changeStatus($consulta,$andamento);
        $response = [
            'message' => 'Consulta Alterada com sucesso.',
            'data' => $data
        ];

        return $response;
    }

    public function getHorarios()
    {
       $consultas = Consulta::query()->with('horario')->get()->toArray();
       return $consultas;
    }
}
