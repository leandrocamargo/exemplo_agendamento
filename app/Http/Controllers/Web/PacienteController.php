<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Historico;
use App\Models\Paciente;
use Illuminate\Http\Request;

class PacienteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('paciente.index');
//        return $paciente = Paciente::query()
//                           ->findOrFail($id)
//                           ->with('convenio')
//                           ->with('documentos')
//                           ->with('consultas')
//                           ->with('historicos')
//                           ->with('historicoAcompanhamentos')
//                           ->with('historicoResultadosExames')
//                           ->where('id','=',$id)
//                           ->get()->toArray();

    }
}
