<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 28/09/2018
 * Time: 10:12
 */

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

class TiposDeConsultasController extends Controller
{
    public function index()
    {
        return view('tipos-consultas.index');
    }
}