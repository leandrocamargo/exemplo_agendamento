<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 01/10/2018
 * Time: 09:41
 */

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\ComboExame;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class AtestadosPDFController extends Controller
{
    public function index (Request $request)
    {
        $exames = json_decode($request->input('exames'),true);

        $atestado = (object) $request->toArray();

        $folhas = DB::table('pivot_combos_exames')
            ->select(DB::raw('DISTINCT (folha)'))
            ->where('combo_exame_id', '=', 1)
            ->orderBy('folha')
            ->get();

        $guiaPdf = [
                    [
                    'folha' => '',
                    'exames' => []
                    ]
                   ];

        $folhas  = $folhas->toArray();
        $aux     = $folhas[0]->{'folha'};
        $index   = 0;
        $guiaPdf[$index]['folha'] = $folhas[0]->{'folha'};

            foreach ($exames as $key => $e) {
                if ($aux == $e['pivot']['folha']){
                   array_push( $guiaPdf[$index]['exames'] , $e['nome']);
                }else{
                    $index ++;
                    $aux = $e['pivot']['folha'];
                    $array = [
                               'folha'  => $aux,
                               'exames' => []
                             ];
                    array_push($array['exames'],$e['nome']);
                    $guiaPdf[$index] = $array;
                }
            }

            $pdf = PDF::loadView('atestado.index', compact('atestado','guiaPdf'), [],
                [
                    'format' => 'A4-L',
                    'margin_left' => 3,
                    'margin_right' => 3,
                    'margin_top' => 3,
                    'margin_bottom' => 3,
                    'tempDir' => public_path('storage/temp')
        ]);
        return $pdf->stream('atestado-simples.pdf');
    }

    public function atestado(Request $request)
    {
        $atestado = (object) $request->toArray();
        $data = new Carbon();
        $data = $data->day . "/" . $data->month . "/" . $data->year;
        $pdf = PDF::loadView('pdf.atestado',
            compact('atestado','data'),[],['format'=>'A4']);

        $pdf->stream('atestado.pdf');
    }

    public function atestadoSimples()
    {
        $data = [];
        $pdf = PDF::loadView('pdf.atestado-simples', $data[], ['format'=>'A4']);

        $pdf->stream('atestado-simples.pdf');
    }

    public function licencaGestante(Request $request)
    {
        $atestado = (object) $request->toArray();
        $pdf = PDF::loadView('pdf.licenca-gestante', compact('atestado'),[],['format'=>'A4']);

        $pdf->download('licenca-gestante.pdf');
    }

    public function atestadoEspecial(Request $request)
    {
        $atestado = (object) $request->toArray();
        $pdf = PDF::loadView('pdf.atestado-especial', compact('atestado', [],['format'=>'A4']));

        return $pdf->stream('atestado-especial.pdf');
    }
}