<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 01/10/2018
 * Time: 09:41
 */

namespace App\Http\Controllers\Web;


use App\Http\Controllers\Controller;
use App\Models\Acompanhamento;
use App\Models\Medicamento;
use Illuminate\Http\Request;

class AcompanhamentoController extends Controller
{
    public function index($id)
    {
        $acompanhamento = Acompanhamento::query()
            ->where('historico_id','=',$id)
            ->get()
            ->all();
        $array = [
            'dados'=>[],
            'label'=>[]
        ];

        $index = 0;

        foreach ($acompanhamento as $chave => $valor){
            $array ['dados'][$index]['peso'] = $valor->peso;
            $array ['dados'][$index]['pa'] = $valor->pa;
            $array ['dados'][$index]['id_gestacao'] = $valor->id_gestacao;
            $array ['label'][$index] = $valor->data;
            $index++;
        }

        return $array;
    }

    public function ajaxMedicamentos()
    {
        $medicamentos = Medicamento::paginate(5);
        $paciente     = Paciente::all();
        if(\Illuminate\Support\Facades\Request::ajax()){
            return view('medicamentos.medicamentos-list.list', compact('pacientes','medicamentos'));
        }
    }
}