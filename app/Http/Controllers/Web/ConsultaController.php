<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Consulta;
use App\Models\Historico;
use App\Models\Paciente;
use App\Models\User;
use App\Traits\Mascara;
use Artesaos\Defender\Role;
use App\Traits\CurrentUser;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ConsultaController extends Controller
{

    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {
        $user = CurrentUser::get();
        $role = $user->role->name;
        if ($role === "ADMIN") {
          $doutores = Role::query()
            ->with('users')
            ->where('name','=',User::DOUTOR)->get()->toArray();
          return view('consultas.index',compact('doutores'));
        } else {
          return view('consultas.index-doctor');
        }

    }

    public function consultasByDoctor ($id, $data)
    {
        $consultas = Consulta::query()
            ->with('andamento')
            ->with('status')
            ->with('paciente')
            ->where('doutor_id','=', $id)
            ->where('data_hora','LIKE','%'.$data.'%')
            ->get();
        return $consultas;
    }

    public function consultasGetPaciente($rg)
    {
        $paciente = Paciente::where('rg','LIKE',"%".$rg."%")->first();
        return $paciente;
    }

    public function consultas()
    {
        return view('horario.index');
    }

    public function getAvailableDates ($doctorId)
    {
        $lotado     = [];
        $disponivel = [];

        $consultas = Consulta::all()
            ->where('data_hora','>', Carbon::now()->subDays(1))
            ->where('doutor_id','=', $doctorId)
            ->where('andamento','<>', 4)
            ->groupBY(function ($data) {
                return Carbon::parse($data->data_hora)->format('d-m-Y');
            });

        foreach ($consultas->toArray() as $key => $value) {
            if (sizeof($consultas[$key]) >= 20) {
                array_push($lotado,$key);
            } else if (sizeof($consultas[$key]) <= 19 && sizeof($consultas[$key]) >=15) {
                array_push($disponivel,$key);
            }
        }

        $data = [
            "lotado"     => $lotado,
            "disponivel" => $disponivel,
        ];

        $response = [
            'message' => 'Consulta Alterada com sucesso.',
            'data' => $data
        ];

        return $response;
    }
}
