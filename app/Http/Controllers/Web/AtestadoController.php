<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 01/10/2018
 * Time: 09:41
 */

namespace App\Http\Controllers\Web;


use App\Http\Controllers\Controller;
use App\Models\Medicamento;

class AtestadoController extends Controller
{
    public function index(Request $request)
    {
        dd($request->all());
        $paciente = Paciente::all();
        $medicamentos = Medicamento::paginate(5);
        $atestado = $request->all();

        return view('atestado.index',compact('pacientes','medicamentos','atestado'));
    }

    public function ajaxMedicamentos()
    {
        $medicamentos = Medicamento::paginate(5);
        $paciente     = Paciente::all();
        if(\Illuminate\Support\Facades\Request::ajax()){
            return view('medicamentos.medicamentos-list.list', compact('pacientes','medicamentos'));
        }
    }
}