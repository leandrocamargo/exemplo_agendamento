<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MedicamentosController extends Controller
{
    public function index()
    {
        return view('medicamentos.index');
    }
}
