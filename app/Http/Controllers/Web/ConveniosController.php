<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

class ConveniosController extends Controller
{

    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {
        return view('convenios.index');
    }

}
