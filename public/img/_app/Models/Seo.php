<?php

class Seo
{
    private $pagina;

    function __construct($pagina){
        $this->pagina = $pagina;

        $this->setMeta();
    }

    public function setMeta(){

        $tabela = '';
        $tableName = '';
        $id = null;
        $row = array();


        if(!is_null($this->pagina)){
            $tabela = explode('/', $this->pagina);
            $id = $tabela[count($tabela) - 1];
        }


        if(ENVIRONMENT == 'production'){

            switch($tabela[1]){
                case 'noticia':
                    $tableName = 'news';
                    break;
                case 'evento':
                    $tableName = 'events';
                    break;
                case '1-feira-empreenda':
                    $tableName = "feira";
                    break;
                default :
                    echo "<meta property=\"og:url\"           content=\"http://www.empreendarevista.com.br\" />
                            <meta property=\"og:type\"          content=\"website\" />
                            <meta property=\"og:title\"         content=\"Empreenda Revista\" />
                            <meta property=\"og:description\"   content=\"Página principal do site Empreenda Revista\" />
                            <meta property=\"og:image\"         content=\"\" />";
                    break;
            }

            if($tableName != ''){

                if($tableName != 'feira'){
                    $read = new Read();
                    $read->ExeRead($tableName, 'WHERE id ='.$id);
                    if($read->getRowCount()>0){
                        $row = $read->getResult();
                    }
                }

                if($tableName != 'events' && $tableName != 'feira'){
                    echo "<meta property=\"og:url\"     content=\"http://{$this->pagina}\" />
                <meta property=\"og:type\"          content=\"website\" />
                <meta property=\"og:title\"         content='".trim($row[0]['title'])."'/>
                <meta property=\"og:description\"   content='".Check::Words(strip_tags($row[0]['news']), 10, false)."' />
                <meta property=\"og:image\"         content='".URL_BASE."/_uploads/".$row[0]['thumb']."' />";
                } elseif($tableName == 'feira'){
                    echo "<meta property=\"og:url\"     content=\"http://{$this->pagina}\" />
                        <meta property=\"og:type\"          content=\"website\" />
                        <meta property=\"og:title\"         content='1º Feira Empreenda+ Mauá, representa além de uma grande Vitrine para quem almeja fixar sua Marca, Uma Oportunidade de conexão com demais empresas e pessoas de Mauá e Região'/>
                        <meta property=\"og:description\"   content='A Feira Empreenda+' />
                        <meta property=\"og:image\"         content='".URL_BASE."/_uploads/1-feira-empreenda.png' />";
                } else {
                    echo "<meta property=\"og:url\"     content=\"http://{$this->pagina}\" />
                <meta property=\"og:type\"          content=\"website\" />
                <meta property=\"og:title\"         content='".trim($row[0]['title'])."'/>
                <meta property=\"og:description\"   content='".Check::Words(strip_tags($row[0]['text']), 10, false)."' />
                <meta property=\"og:image\"         content='".URL_BASE."/_uploads/eventos/".$row[0]['thumb']."' />";
                }
            }

        } else {

        }

    }
}