var id = 2;

$(document).ready(function ($) {
    getAvailableDates (id);
    $('.table-condensed').addClass('text-center');
    $('#doutor-select').on('change', function () {
        $('#calendar-agenda').datepicker("destroy");
        getAvailableDates($('#doutor-select').val());
    });

});

function getAvailableDates (id) {
    $.ajax({
        url: '/consulta-get-available-dates/'+id,
        method: 'get',
        success: function (response) {
            console.log('/consulta-get-available-dates/', response);
            disponivel = response.data.disponivel;
            lotado     = response.data.lotado;

            _lotado     = 0;
            _disponivel = 0;

            $('#calendar-agenda').datepicker({
                format: 'DD/MM/yyyy',
                language: 'pt-BR',
                todayHighlight: false,
                beforeShowDay: function (date) {
                    for (var verificar = 0; verificar <= 366; verificar++) {

                        var _date = moment(date,'DD-MM-YYYY').format('DD-MM-YYYY');
                        if ( _date === lotado[_lotado]) {
                            _lotado ++;
                            return {classes: 'bg-danger', tooltip: 'Lotado'};
                        }
                        else if ( _date === disponivel[_disponivel]) {
                            _disponivel ++;
                            return {classes: 'bg-warning', tooltip: 'Ainda há vagas'};
                        }
                        else {
                            return [true]
                        }
                    }
                }
            }).on('changeMonth', function(e) {
                getAvailableDates (id);
            });
            $('.table-condensed').addClass('text-center');
        }
    });
}