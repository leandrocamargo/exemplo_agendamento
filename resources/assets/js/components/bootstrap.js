require('./shared/_register');
Vue.component('example', require('./Example'));
Vue.component('consulta', require('./consulta/Consulta'));
Vue.component('doutor', require('./doutor/Doutor'));
Vue.component('tabela-horarios', require('./horarios/tabela-horarios'));
Vue.component('exames', require('./exames/Exame'));
Vue.component('medicamentos', require('./medicamentos/Medicamento'));
Vue.component('convenio', require('./convenios/Convenio'));
Vue.component('tipos-consultas', require('./tipos-consultas/TiposDeConsultas'));
Vue.component('paciente', require('./pacientes/Pacientes'))
///Vue.component('calendar', require('./consulta/calendario-consulta/calendario-consulta'));

