@extends('adminlte::page')

@section('title', 'La Gravidanza')

@section('content_header')
    <div class="row">
        <div class="col-lg-12">
            <section class="content-header">
                <h1>
                    <h1>Consulta</h1>
                    <small>Gerenciamento de Consultas</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Consultas</li>
                </ol>
            </section>
            <section class="content-header">
                @php
                    setlocale(LC_ALL, 'portuguese');
                    date_default_timezone_set('America/Sao_Paulo');
                    $date = date('Y-m-d');
                    $date_extenso = utf8_encode(strftime("%A, %d de %B de %Y", strtotime($date)));
                @endphp
                <strong class="box-title">{{$date_extenso}}</strong>
            </section>
        </div>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-8">
          <consulta></consulta>
        </div>
        <div class="col-lg-4">
            <div class="box box-solid">
                <div class="box-header text-center">
                    <i class="fa fa-calendar"></i>

                    <h3 class="box-title">Agenda</h3>
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                    </div>
                    <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <!--The calendar -->
                    <div id="calendar-agenda" style="width: 100%"></div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@stop