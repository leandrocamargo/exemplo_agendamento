@extends('adminlte::page')

@section('title', 'La Gravidanza')

@section('content_header')
    <h1>Consultas</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <consulta></consulta>
        </div>
    </div>
@stop