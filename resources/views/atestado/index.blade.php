<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <title>Guia</title>
</head>
<body>
@foreach($guiaPdf as $folha => $f)
    @php $var = implode(' / ',$f['exames']);@endphp
    <page class="tela">
        <p>GUIA DE SERVIÇO PROFISSIONAL / SERIÇO AUXILIAR DE DIAGNÓTICO E TERAPIA - SP/SADT</p>

        <div id="linha1">
            <div class="regAns">1-Registro Ans</div>

            <div class="guiaPrinc">3-Nº Guia Principal</div>

            <div class="dtsAuto">4-Data da Autorização</div>

            <div class="senha">5-Senha </div>

            <div class="dtsValidSenha">6-Data Validade da Senha</div>

            <div class="dtsEmissGuia">7-Data de Emissão da Guia</div>
        </div>

        <div id="linha2">&nbsp;&nbsp;&nbsp;Dados do Beneficiário
            <div class="numCarteira">8-Número da Carteira</div>

            <div class="plano">9-Plano</div>

            <div class="validCart">10-Validade da Carteira</div>

            <div class="nome">11-Nome
                <!-- $atestado->nomePaci -->
                <div class="conteNome"></div>
            </div>

            <div class="cartNacSaude">12-Número do cartão Nacional de Saúde</div>
        </div>

        <div id="linha3">&nbsp;&nbsp;&nbsp;Dados do contratado Solicitante
            <div class="codOper">13-Código na Operqadora / CNPJ / CPF
                <div class="conteCodOpera">02923439000111</div>
            </div>

            <div class="nomeContratado">14-Nome no Contratado
                <div class="conteNomeContratado">LA GRAVIDANZA CLINICA MEDICA SS LTDA</div>
            </div>

            <div class="codCnes">15-Código CNES
                <div class="conteCodCens">6146783</div>
            </div>
        </div>

        <div id="linha4">
            <div class="nomeProfiSoli">16-Nome do Profissional Solicitante
                <!-- $atestado->nomeProfi -->
                <div class="conteNomeProfiSoli">{{$atestado->profissional}}</div>
            </div>
            <div class="consProfi">17-Conselho Profissional
                <div class="conteConsProfi">CRM</div>
            </div>
            <div class="numCons">18-Número no Conselho
                <!-- $atestado->numCons -->
                <div class="conteNumCons">{{$atestado->crm}}</div>
            </div>
            <div class="uf">19-UF
                <div class="conteUf">SP</div>
            </div>
            <div class="codCBO">20-Código CBO S
                <div class="conteCodCbo">225250</div>
            </div>
        </div>

        <div id="linha5">&nbsp;&nbsp;&nbsp;Dados da Solicitção / Procedimento e Exames Solicitantes
            <div class="datHorSolic">21-Data/Hora da Solicitação</div>
            <div class="caraterSolic">22-Caráter da Solicitação</div>
            <div class="cid">23-CID 10</div>
            <div class="indiClinc">24-Indicação Clínica(obrigatório se pequena círurgia, terapia, consulta de referência e alto custo)</div>
        </div>

        <div id="linha6">
            <div id="linha6a">
                <div class="tabela">25-Tabela</div>
                <div class="codProced">26-Código do Procedimento</div>
                <div class="descricao">27-Descrição
                    <!-- $atestado->exame1 / $atestado->exame2 / $atestado->exame3-->
                    <div class="conteDesc"> {{ $var }} </div>
                </div>
                <div class="qtSolic">28-Qt. Solic.</div>
                <div class="qtAutoriz">29-Qt. Autoriz</div>
            </div>

            <div id="linha6b">
                <div class="tabela"></div>
                <div class="codProced"></div>
                <div class="descricao"></div>
                <div class="qtSolic"></div>
                <div class="qtAutoriz"></div>
            </div>

            <div id="linha6c">
                <div class="tabela"></div>
                <div class="codProced"></div>
                <div class="descricao"></div>
                <div class="qtSolic"></div>
                <div class="qtAutoriz"></div>
            </div>

            <div id="linha6d">
                <div class="tabela"></div>
                <div class="codProced"></div>
                <div class="descricao"></div>
                <div class="qtSolic"></div>
                <div class="qtAutoriz"></div>
            </div>

            <div id="linha6e">
                <div class="tabela"></div>
                <div class="codProced"></div>
                <div class="descricao"></div>
                <div class="qtSolic"></div>
                <div class="qtAutoriz"></div>
            </div>
        </div>

        <div id="linha7">&nbsp;&nbsp;&nbsp;Dados da Contratado Executante
            <div class="codOperContratado">30-Código na Operadora / CNPJ / CPF</div>
            <div class="nomeContratado2">31-Nome do Contratado</div>
            <div class="tl">32-T.L.</div>
            <div class="lograNumCompl">33-34-35-Logradouro - Número - Complemento</div>
            <div class="municipio">36-Município</div>
            <div class="ufContratado">37-UF</div>
            <div class="codIbge">38-Cód. IBGE</div>
            <div class="cepContratado">39-CEP</div>
            <div class="codCnesContratado">40-Código CNES</div>
        </div>

        <div id="linha8">
            <div class="codExecCompl">40a-Código na Operadora / CPF do exex. complementar</div>
            <div class="nomeProfiExecu">41-Nome no profissional Executante Complementar</div>
            <div class="consProfi2">42-Conselho Profissional</div>
            <div class="numCons2">43-Número no Conselho</div>
            <div class="ufContratado2">44-UF</div>
            <div class="codCBO2">45-Código CBO S</div>
            <div class="grauPart">45a-Grau de Participação</div>
        </div>

        <div id="linha9">&nbsp;&nbsp;&nbsp;Dados do Atendimento
            <div class="tipoAtendim">45-Tipo Atendimento</div>
            <div class="indcAcidente">47-Indicação de Acidente</div>
            <div class="tipoSaida">48-Tipo de Saída</div>
        </div>

        <div id="linha10">&nbsp;&nbsp;&nbsp;Consulta Referência
            <div class="tipoDoenca">49-Tipo de Doença</div>
            <div class="tempDoenca">50-Tempo de Doença</div>
        </div>

        <div id="linha11">
            <div id="linha11a">
                <div class="dataProcedExame">51-Data</div>
                <div class="horaInic">52-Hora Inicial</div>
                <div class="horaFinal">53-Hora Final</div>
                <div class="tabelaProcedExame">54-Tabela</div>
                <div class="codProcedExame">55-Código do Procedimento</div>
                <div class="descProcedExame">56-Descrição</div>
                <div class="qtdeProceExame">57-Qtde.</div>
                <div class="via">58-via</div>
                <div class="tec">59-tec.</div>
                <div class="porcenRedAcresc">60-% Red/Acresc</div>
                <div class="valorUni">61-Valor Unitário-R$</div>
                <div class="valorTotal">62-Valor Total-R$</div>
            </div>

            <div id="linha11b">
                <div class="dataProcedExame"></div>
                <div class="horaInic"></div>
                <div class="horaFinal"></div>
                <div class="tabelaProcedExame"></div>
                <div class="codProcedExame"></div>
                <div class="descProcedExame"></div>
                <div class="qtdeProceExame"></div>
                <div class="via"></div>
                <div class="tec"></div>
                <div class="porcenRedAcresc"></div>
                <div class="valorUni"></div>
                <div class="valorTotal"></div>
            </div>

            <div id="linha11c">
                <div class="dataProcedExame"></div>
                <div class="horaInic"></div>
                <div class="horaFinal"></div>
                <div class="tabelaProcedExame"></div>
                <div class="codProcedExame"></div>
                <div class="descProcedExame"></div>
                <div class="qtdeProceExame"></div>
                <div class="via"></div>
                <div class="tec"></div>
                <div class="porcenRedAcresc"></div>
                <div class="valorUni"></div>
                <div class="valorTotal"></div>
            </div>

            <div id="linha11d">
                <div class="dataProcedExame"></div>
                <div class="horaInic"></div>
                <div class="horaFinal"></div>
                <div class="tabelaProcedExame"></div>
                <div class="codProcedExame"></div>
                <div class="descProcedExame"></div>
                <div class="qtdeProceExame"></div>
                <div class="via"></div>
                <div class="tec"></div>
                <div class="porcenRedAcresc"></div>
                <div class="valorUni"></div>
                <div class="valorTotal"></div>
            </div>

            <div id="linha11e">
                <div class="dataProcedExame"></div>
                <div class="horaInic"></div>
                <div class="horaFinal"></div>
                <div class="tabelaProcedExame"></div>
                <div class="codProcedExame"></div>
                <div class="descProcedExame"></div>
                <div class="qtdeProceExame"></div>
                <div class="via"></div>
                <div class="tec"></div>
                <div class="porcenRedAcresc"></div>
                <div class="valorUni"></div>
                <div class="valorTotal"></div>
            </div>
        </div>

        <div id="linha12">63-Data e Assinaturade Procedimento em Série
            <div id="linha12a">
                <div class="primeiradt">1</div>
                <div class="primeiraass"></div>

                <div class="terceiradt">3</div>
                <div class="terceiraass"></div>

                <div class="quintadt">5</div>
                <div class="quintaass"></div>

                <div class="setimadt">7</div>
                <div class="setimaass"></div>

                <div class="nonadt">9</div>
                <div class="nonaass"></div>
            </div>

            <div id="linha12b">
                <div class="segundadt">2</div>
                <div class="segundaass"></div>

                <div class="quartadt">4</div>
                <div class="quartaass"></div>

                <div class="sextadt">6</div>
                <div class="sextaass"></div>

                <div class="oitavadt">8</div>
                <div class="oitavaass"></div>

                <div class="decimadt">10</div>
                <div class="decimaass"></div>
            </div>
        </div>

        <div id="linha13">64-Observação</div>

        <div id="linha14">
            <div class="totalProced">65-Total Procedimento R$</div>
            <div class="totalTaxaAlug">66-Total Taxas e Aluguéis R$</div>
            <div class="totalMate">67-Total Materiais R$</div>
            <div class="totalMedica">68-Total Medicamento R$</div>
            <div class="totalDiaria">69-Total Diária R$</div>
            <div class="totalGases">70-Total Gases Medicinais R$</div>
            <div class="totalGeral">71-Total Geral da Guia</div>
        </div>

        <div id="linha15">
            <div class="dataAssSolic">72-Data e Assinatura do Solicitante</div>
            <div class="dataAssRespo">73-Data e Assinatura do Responsável pela Autorização</div>
            <div class="dataAssBenef">74-Data e Assinatura do Beneficiário ou Responsável</div>
            <div class="dataAssPrest">75-Data e Assinatura do Prestador Executante</div>
        </div>
    </page>
@endforeach
</body>
<style type="text/css">
    .tela {
        border-style: solid;
        border-width: thin;
        width: 100%;
        height: 100%;
        text-align: center;
    }
    /*Começa a linha 1*/
    .regAns{
        border-style: solid;
        border-width: thin;
        width: 150px;
        height: 25px;
        float: left;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        margin: 4px 3px 4px 4px;
        position: relative;
    }
    .guiaPrinc{
        border-style: solid;
        border-width: thin;
        width: 240px;
        height: 25px;
        float: left;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        margin-left: 3px;
        position: relative;
    }
    .dtsAuto {
        border-style: solid;
        border-width: thin;
        width: 120px;
        height: 25px;
        float: left;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        margin-left: 3px;
        position: relative;
    }
    .senha {
        border-style: solid;
        border-width: thin;
        width: 100px;
        height: 25px;
        float: left;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        margin-left: 3px;
        position: relative;
    }
    .dtsValidSenha {
        border-style: solid;
        border-width: thin;
        width: 120px;
        height: 25px;
        float: left;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        margin-left: 3px;
        position: relative;
    }
    .dtsEmissGuia {
        border-style: solid;
        border-width: thin;
        width: 120px;
        height: 25px;
        float: left;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        margin-left: 3px;
        position: relative;
    }
    /*Começa a linha 2*/
    #linha2{
        font-size: 6px;
        text-align: left;
    }
    .numCarteira{
        border-style: solid;
        border-width: thin;
        width: 240px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .plano {
        border-style: solid;
        border-width: thin;
        width: 90px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .validCart {
        border-style: solid;
        border-width: thin;
        width: 120px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .nome{
        border-style: solid;
        border-width: thin;
        width: 260px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .conteNome{
        text-align: left;
        font-size: 10px;
        float: bottom;
        margin-left: 3px;
    }
    .cartNacSaude{
        border-style: solid;
        border-width: thin;
        width: 245px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    /*Começa a linha 3*/
    #linha3{
        font-size: 6px;
        text-align: left;
        margin-bottom: 3px;
    }
    .conteCodOpera{
        text-align: left;
        font-size: 10px;
        float: bottom;
        margin-left: 3px;
    }
    .codOper{
        border-style: solid;
        border-width: thin;
        table-layout: fixed;
        width: 200px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .nomeContratado{
        border-style: solid;
        border-width: thin;
        width: 280px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .conteNomeContratado{
        text-align: left;
        font-size: 10px;
        float: bottom;
        margin-left: 3px;
    }
    .codCnes{
        border-style: solid;
        border-width: thin;
        width: 90px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .conteCodCens{
        text-align: left;
        font-size: 10px;
        float: bottom;
        margin-left: 3px;
    }
    /*Começa a linha 4*/
    .nomeProfiSoli{
        border-style: solid;
        border-width: thin;
        width: 260px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .conteNomeProfiSoli{
        text-align: left;
        font-size: 10px;
        float: bottom;
        margin-left: 3px;
    }
    .consProfi{
        border-style: solid;
        border-width: thin;
        width: 160px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .conteConsProfi{
        text-align: left;
        font-size: 10px;
        float: bottom;
        margin-left: 3px;
    }
    .numCons{
        border-style: solid;
        border-width: thin;
        width: 160px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .conteNumCons{
        text-align: left;
        font-size: 10px;
        float: bottom;
        margin-left: 3px;
    }
    .uf{
        border-style: solid;
        border-width: thin;
        width: 70px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .conteUf{
        text-align: left;
        font-size: 10px;
        float: bottom;
        margin-left: 3px;
    }
    .codCBO{
        border-style: solid;
        border-width: thin;
        width: 100px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .conteCodCbo{
        text-align: left;
        font-size: 10px;
        float: bottom;
        margin-left: 3px;
    }
    /*Começa a linha 5*/
    #linha5{
        font-size: 6px;
        text-align: left;
        margin-bottom: 3px;
    }
    .datHorSolic{
        border-style: solid;
        border-width: thin;
        width: 200px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .caraterSolic{
        border-style: solid;
        border-width: thin;
        width: 130px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .cid{
        border-style: solid;
        border-width: thin;
        width: 100px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .indiClinc{
        border-style: solid;
        border-width: thin;
        width: 534px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    /*Começa a linha 6*/
    #linha6{
        border-style: solid;
        border-width: thin;
        width: 99%;
        height: 100px;
        margin-left: 3px;
    }
    .tabela{
        border-bottom: solid;
        border-width: thin;
        width: 23px;
        height: 20px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .codProced{
        border-bottom: solid;
        border-width: thin;
        width: 140px;
        height: 20px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .descricao{
        border-bottom: solid;
        border-width: thin;
        width: 650px;
        height: 20px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 10px;
    }
    .conteDesc{
        text-align: left;
        font-size: 10px;
        float: bottom;
        margin-left: 3px;
    }
    .qtSolic{
        border-bottom: solid;
        border-width: thin;
        width: 35px;
        height: 20px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 10px;
    }
    .qtAutoriz{
        border-bottom: solid;
        border-width: thin;
        width: 35px;
        height: 20px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    #linha6e{
        margin-bottom: 3px;
    }
    /*Começa a linha 7*/
    #linha7{
        font-size: 6px;
        text-align: left;
        margin-bottom: 1px;
    }
    .codOperContratado{
        border-style: solid;
        border-width: thin;
        width: 200px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .nomeContratado2{
        border-style: solid;
        border-width: thin;
        width: 280px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .tl{
        border-style: solid;
        border-width: thin;
        width: 23px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .lograNumCompl{
        border-style: solid;
        border-width: thin;
        width: 160px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .municipio{
        border-style: solid;
        border-width: thin;
        width: 90px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .ufContratado{
        border-style: solid;
        border-width: thin;
        width: 20px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .codIbge{
        border-style: solid;
        border-width: thin;
        width: 50px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .cepContratado{
        border-style: solid;
        border-width: thin;
        width: 45px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .codCnesContratado{
        border-style: solid;
        border-width: thin;
        width: 50px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    /*Começa a linha 8*/
    #linha8{
        font-size: 6px;
        text-align: left;
    }
    .codExecCompl{
        border-style: solid;
        border-width: thin;
        width: 200px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .nomeProfiExecu{
        border-style: solid;
        border-width: thin;
        width: 280px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .consProfi2{
        border-style: solid;
        border-width: thin;
        width: 120px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .numCons2{
        border-style: solid;
        border-width: thin;
        width: 110px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .ufContratado2{
        border-style: solid;
        border-width: thin;
        width: 20px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .codCBO2{
        border-style: solid;
        border-width: thin;
        width: 100px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .grauPart{
        border-style: solid;
        border-width: thin;
        width: 106px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    /*Começa a linha 9*/
    #linha9{
        font-size: 6px;
        text-align: left;
    }
    .tipoAtendim{
        border-style: solid;
        border-width: thin;
        width: 280px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .indcAcidente{
        border-style: solid;
        border-width: thin;
        width: 240px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .tipoSaida{
        border-style: solid;
        border-width: thin;
        width: 260px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    /*Começa a linha 10*/
    #linha10{
        font-size: 6px;
        text-align: left;
        margin-bottom: 3px;
    }
    .tipoDoenca{
        border-style: solid;
        border-width: thin;
        width: 100px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .tempDoenca{
        border-style: solid;
        border-width: thin;
        width: 130px;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    /*Começa a linha 11*/
    #linha11{
        border-style: solid;
        border-width: thin;
        width: 99%;
        height: 120px;
        margin-left: 3px;
        margin-bottom: 3px;
    }
    .dataProcedExame{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 18px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .horaInic{
        border-bottom: solid;
        border-width: thin;
        width: 60px;
        height: 18px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .horaFinal{
        border-bottom: solid;
        border-width: thin;
        width: 60px;
        height: 18px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .tabelaProcedExame{
        border-bottom: solid;
        border-width: thin;
        width: 23px;
        height: 18px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .codProcedExame{
        border-bottom: solid;
        border-width: thin;
        width: 140px;
        height: 18px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .descProcedExame{
        border-bottom: solid;
        border-width: thin;
        width: 190px;
        height: 18px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .qtdeProceExame{
        border-bottom: solid;
        border-width: thin;
        width: 23px;
        height: 18px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .via{
        border-bottom: solid;
        border-width: thin;
        width: 20px;
        height: 18px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .tec{
        border-bottom: solid;
        border-width: thin;
        width: 20px;
        height: 18px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .porcenRedAcresc{
        border-bottom: solid;
        border-width: thin;
        width: 50px;
        height: 18px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .valorUni{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 18px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .valorTotal{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 18px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    /*Começa a linha 12*/
    #linha12{
        border-style: solid;
        border-width: thin;
        width: 99%;
        height: 40px;
        margin-left: 3px;
        font-size: 5px;
        text-align: left;
        margin-bottom: 1px;
    }
    .primeiradt{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .primeiraass{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .terceiradt{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .terceiraass{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .quintadt{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .quintaass{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .setimadt{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .setimaass{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .nonadt{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .nonaass{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    #linha12b{
        margin-bottom: 3px;
    }
    .segundadt{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .segundaass{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .quartadt{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .quartaass{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .sextadt{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .sextaass{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .oitavadt{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .oitavaass{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .decimadt{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    .decimaass{
        border-bottom: solid;
        border-width: thin;
        width: 80px;
        height: 13px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 8px;
    }
    /*Começa a linha 13*/
    #linha13{
        border-style: solid;
        border-width: thin;
        width: 99%;
        height: 30px;
        margin-left: 3px;
        font-size: 5px;
        text-align: left;
        margin-bottom: 3px;
    }
    /*Começa a linha 14*/
    #linha14{
        margin-bottom: 1px;
    }
    .totalProced{
        border-style: solid;
        border-width: thin;
        width: 13%;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .totalTaxaAlug{
        border-style: solid;
        border-width: thin;
        width: 13%;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .totalMate{
        border-style: solid;
        border-width: thin;
        width: 13%;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .totalMedica{
        border-style: solid;
        border-width: thin;
        width: 13%;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .totalDiaria{
        border-style: solid;
        border-width: thin;
        width: 14%;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .totalGases{
        border-style: solid;
        border-width: thin;
        width: 14%;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .totalGeral{
        border-style: solid;
        border-width: thin;
        width: 13.66%;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    /*Começa a linha15*/
    .dataAssSolic{
        border-style: solid;
        border-width: thin;
        width: 24%;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .dataAssRespo{
        border-style: solid;
        border-width: thin;
        width: 24%;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .dataAssBenef{
        border-style: solid;
        border-width: thin;
        width: 24%;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
    .dataAssPrest{
        border-style: solid;
        border-width: thin;
        width: 24.1%;
        height: 25px;
        font-size: 5px;
        text-align: left;
        padding: 2px;
        float: left;
        position: relative;
        margin-left: 3px;
    }
</style>
</html>
