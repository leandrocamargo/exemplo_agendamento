@extends('adminlte::page')

@section('title', 'La Gravidanza')

@section('content_header')
    <div class="row">
        <div class="col-lg-12">
           <section class="content-header">
               <h1>
                   <h1>Medicamentos</h1>
                   <small>Cadastro de Medicamentos</small>
               </h1>
               <ol class="breadcrumb">
                   <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                   <li class="active">Medicamentos</li>
               </ol>
           </section>
           <section class="content-header">
              @php
                  setlocale(LC_ALL, 'portuguese');
                  date_default_timezone_set('America/Sao_Paulo');
                  $date = date('Y-m-d');
                  $date_extenso = utf8_encode(strftime("%A, %d de %B de %Y", strtotime($date)));
              @endphp
               <strong class="box-title">{{$date_extenso}}</strong>
           </section>
        </div>
    </div>
@stop

@section('content')
    <div class="row">
        <medicamentos></medicamentos>
    </div>
@stop