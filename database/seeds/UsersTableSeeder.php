<?php

use App\Models\User;
use Artesaos\Defender\Facades\Defender;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createAdmin();
        $this->createDoutor();
        $this->createUsers();
        $this->createDoctors();
    }

    private function createAdmin ()
    {
        User::create([
            'name'     => 'Admin',
            'email'    => 'admin@admin.com',
            'password' => bcrypt('123456'),
            'is_deleted' => false
        ])->attachRole(Defender::findRole(User::ADMIN));
    }

    private function createDoutor ()
    {
        User::create([
            'name'     => 'Doutor',
            'email'    => 'doutor@doutor.com',
            'password' => bcrypt('123456'),
            'is_deleted' => false
        ])->attachRole(Defender::findRole(User::DOUTOR));
    }

    private function createUsers ()
    {
       factory(User::class, 5)->create()->each(function (User $user){
           $user->attachRole(Defender::findRole(User::USER));
       });
    }

    private function createDoctors ()
    {
        factory(User::class, 5)->create()->each(function (User $user){
            $user->attachRole(Defender::findRole(User::DOUTOR));
        });
    }
}
