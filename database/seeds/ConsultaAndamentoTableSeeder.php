<?php

use App\Models\ConsultaAndamento;
use Illuminate\Database\Seeder;

class ConsultaAndamentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [
            'Agendado',
            'Atendido',
            'Não Compareceu',
            'Paciente Desmarcou',
            'Aguardando'
        ];

        $icones = [
            '<i style="color: #FBC02D; font-size: 18px;" class="fa fa-calendar-check-o" aria-hidden="true"></i>',
            '<i style="color: #2E7D32; font-size: 18px;" class="fa fa-smile-o" aria-hidden="true"></i>',
            '<i style="color: #F44336; font-size: 18px;" class="fa fa-frown-o" aria-hidden="true"></i>',
            '<i style="color: #2196F3; font-size: 18px;" class="fa fa-meh-o" aria-hidden="true"></i>',
            '<i style="color: #11a32a; font-size: 18px;" class="fa fa-thumbs-up" aria-hidden="true"></i>',
        ];

        for ($i = 0 ; $i < sizeof($status); $i++) {
            ConsultaAndamento::create([
                'nome' => $status[$i],
                'icone' => $icones[$i],
            ]);
        }
    }
}
