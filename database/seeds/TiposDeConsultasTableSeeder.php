<?php

use App\Models\TipoDeConsulta;
use Illuminate\Database\Seeder;

class TiposDeConsultasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->createTiposDeConsulta();
    }

    private function createTiposDeConsulta () {
        $tipos = [
            'Pré Natal 1',
            'Pré Natal 2',
            'Pré Natal 3',
            'Pré Natal 4',
            'Pré Natal 5'
        ];

        for ($i = 0 ; $i < sizeof($tipos); $i++) {
            TipoDeConsulta::create([
                'nome' => $tipos[$i]
            ]);
        }
    }
}
