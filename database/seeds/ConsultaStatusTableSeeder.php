<?php

use App\Models\ConsultaStatus;
use Illuminate\Database\Seeder;

class ConsultaStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [
                    'Agendado',
                    'Aguardando',
                    'Cancelado',
                    'Remarcado Cliente',
                    'Remarcado Gravidanza',
                    'Retorno'
                  ];

        for ($i = 0 ; $i < sizeof($status); $i++) {
            ConsultaStatus::create([
                'nome' => $status[$i]
            ]);
        }
    }
}
