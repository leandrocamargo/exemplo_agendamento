<?php

use App\Models\User;
use Artesaos\Defender\Facades\Defender;
use Artesaos\Defender\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Defender::createRole(User::ADMIN);
        Defender::createRole(User::DOUTOR);
        Defender::createRole(User::USER);
    }

    private function createDoctorsRole () {
        Defender::createRole(User::DOUTOR);
        $role = Defender::findRole(User::DOUTOR);
        Defender::createPermission('import.csv');
        $permission = Defender::findPermission('import.csv');
        $role->attachPermission($permission);
    }
}
