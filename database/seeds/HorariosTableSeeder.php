<?php

use App\Models\Horario;
use Illuminate\Database\Seeder;

class HorariosTableSeeder extends Seeder
{

    public function run()
    {
      $this->createTimes();
    }

    public function createTimes()
    {
        for ($hora = 8; $hora < 20; $hora++) {
            $aux = 0;
            for ($min = 0; $min < 2; $min++) {
                Horario::create([
                    'inicio_consulta' => self::gerarHora($hora ,$min),
                    'fim_consulta'    => self::gerarHora($hora + $aux,$min + 1),
                ]);
                $aux = 1;
            }
        }
    }

    private function gerarHora ($hora, $min) {
       $minutos = '';

       if (($min%2) == 0)  {
           $minutos = '00';
       } else {
           $minutos = '30';
       }

       return  str_pad($hora,2,'0',STR_PAD_LEFT) . ":" . $minutos . ":" . "00";
    }
}
