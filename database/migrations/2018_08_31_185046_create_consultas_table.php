<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultas', function (Blueprint $table) {
            $table->increments('id');
            $table->date('data_hora');

            $table->integer('doutor_id')->unsigned();
            $table->foreign('doutor_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('horario_id')->unsigned();
            $table->foreign('horario_id')
                ->references('id')
                ->on('horarios')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('tipo_id')->unsigned();
            $table->foreign('tipo_id')
                ->references('id')
                ->on('tipos_de_consulta')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('paciente_id')->unsigned();
            $table->foreign('paciente_id')
                ->references('id')
                ->on('pacientes')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('status')->unsigned()->default(1);
            $table->foreign('status')
                ->references('id')
                ->on('consulta_status')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('andamento')->unsigned()->default(1);
            $table->foreign('andamento')
                ->references('id')
                ->on('consulta_andamento')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultas');
    }
}
