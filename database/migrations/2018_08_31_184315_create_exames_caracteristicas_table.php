<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamesCaracteristicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exames_caracteristicas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome',150);

            $table->integer('exame_id')->unsigned();
            $table->foreign('exame_id')
                ->references('id')
                ->on('exames')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exames_caracteristicas');
    }
}
