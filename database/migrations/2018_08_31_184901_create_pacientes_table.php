<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string    ('matricula', 16)->nullable();
            $table->string    ('nome', 50);
            $table->date      ('data_nascimento')->nullable();
            $table->char      ('sexo', 1)->default('F')->nullable();
            $table->string    ('estado_civil', 20)->nullable();
            $table->string    ('cor', 20)->nullable();
            $table->string    ('naturalidade', 100)->nullable();
            $table->string    ('grau_instrucao', 20)->nullable();
            $table->string    ('rg', 12);
            $table->string    ('orgao_emissor', 6)->nullable();
            $table->string    ('cpf', 12)->nullable();
            $table->string    ('titular_cpf', 50)->nullable();
            $table->string    ('profissao', 50)->nullable();
            $table->string    ('cel', 22)->nullable();
            $table->string    ('fixo', 22)->nullable();
            $table->date      ('previsao_retorno')->nullable();
            $table->string    ('email', 255)->nullable();
            $table->string    ('indicacao', 50)->nullable();
            $table->string    ('cep', 9)->nullable();
            $table->string    ('logradouro', 255)->nullable();
            $table->string    ('complemento', 255)->nullable();
            $table->string    ('bairro', 50)->nullable();
            $table->string    ('cidade', 50)->nullable();
            $table->string    ('uf', 2)->nullable();
            $table->text      ('observacao')->nullable();
            $table->text      ('foto', 255)->nullable();
            $table->integer   ('user_id_created')->unsigned();
            $table->foreign   ('user_id_created')->references('id')->on('users')->onDelete('restrict')->onUpdate('restrict');
            $table->integer   ('user_id_updated')->unsigned();
            $table->foreign   ('user_id_updated')->references('id')->on('users')->onDelete('restrict')->onUpdate('restrict');

            $table->integer('convenio_id')->unsigned();
            $table->foreign('convenio_id')
                ->references('id')
                ->on('convenios')
                ->onDelete('cascade')
                ->onUpdate('cascade')->nullable();

            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
