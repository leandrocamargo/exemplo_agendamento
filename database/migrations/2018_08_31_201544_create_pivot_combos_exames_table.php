<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotCombosExamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pivot_combos_exames', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('folha')->default(1);
            $table->integer('combo_exame_id')->unsigned();
            $table->foreign('combo_exame_id')
                ->references('id')
                ->on('combos_exames')
                ->onDelete('cascade')
                ->onUpdate('cascade')->nullable();

            $table->integer('exame_id')->unsigned();
            $table->foreign('exame_id')
                ->references('id')
                ->on('exames')
                ->onDelete('cascade')
                ->onUpdate('cascade')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pivot_combos_exames');
    }
}
