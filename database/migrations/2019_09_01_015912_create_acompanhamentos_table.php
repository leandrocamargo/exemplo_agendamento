<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcompanhamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acompanhamentos', function (Blueprint $table) {
            $table->increments        ('id');
            $table->date              ('data')->nullable();
            $table->integer           ('id_gestacao')->nullable();
            $table->double            ('peso',2)->nullable();
            $table->integer           ('pa')->nullable();
            $table->string            ('au')->nullable();
            $table->string            ('mf')->nullable();
            $table->string            ('bcf')->nullable();
            $table->string            ('edema')->nullable();

            $table->integer('historico_id')->unsigned();
            $table->foreign('historico_id')
                ->references('id')
                ->on('paciente_historico')
                ->onDelete('cascade')
                ->onUpdate('cascade')->nullable();

            $table->boolean           ('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acompanhamentos');
    }
}
