<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resultados', function (Blueprint $table) {
            $table->increments('id');
            $table->text('valor');

            $table->integer('historico_id')->unsigned();
            $table->foreign('historico_id')
                ->references('id')
                ->on('paciente_historico')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('caracteristica_exame_id')->unsigned();
            $table->foreign('caracteristica_exame_id')
                ->references('id')
                ->on('exames_caracteristicas')
                ->onDelete('cascade')
                ->onUpdate('cascade')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resultados');
    }
}
