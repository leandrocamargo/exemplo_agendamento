<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricoAnotacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historico_anotacoes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('historico_id')->unsigned();
            $table->foreign('historico_id')
                ->references('id')
                ->on('paciente_historico')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->dateTime('data_anotacao')->default(Carbon::now());
            $table->string('anotacao',500);

            $table->boolean('is_deleted')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historico_anotacoes_table');
    }
}
