<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


$factory->define(App\Models\Paciente::class, function (Faker $faker) {

    $sexo        = ['f','m'];
    $estadoCivil = ['casado(a)','solteiro(a)','viúvo(a)'];
    $cor         = ['branco','negro','oriental'];
    $instrucao   = ['fundamental','superior','ensino médio'];
    $rg          = [
                        '33.731.002-6',
                        '46.823.899-2',
                        '44.033.679-3',
                        '46.746.626-9',
                        '16.598.018-7',
                        '42.908.507-2',
                        '27.111.781-3',
                        '29.485.445-9',
                        '46.818.899-2',
                        '46.820.789-9',
                        '46.822.855-2',
                        '46.844.897-7',
                        '46.828.898-7',
                        '46.869.896-3',
                        '46.870.899-7',
                        '46.843.599-4',
                        '46.815.799-7',
                        '46.884.899-2',
                        '46.823.810-1',
                        '46.894.596-7'
                   ];

    return [
        'nome'                    => $faker->name,
        'convenio_id'             => random_int(1,20),
        'matricula'               => $faker->bankAccountNumber,
        'data_nascimento'         => $faker->date('Y-m-d'),
        'sexo'                    => $sexo[random_int(0,1)],
        'estado_civil'            => $estadoCivil[random_int(0,2)],
        'cor'                     => $cor[random_int(0,2)],
        'naturalidade'            => $faker->country,
        'grau_instrucao'          => $instrucao[random_int(0,2)],
        'rg'                      => $rg[random_int(0,19)],
        'orgao_emissor'           => "SSP-SP",
        'cpf'                     => $rg[random_int(0,19)],
        'profissao'               => $faker->jobTitle,
        'previsao_retorno'        => $faker->date('Y-m-d'),
        'email'                   => $faker->email,
        'indicacao'               => $faker->name,
        'logradouro'              => $faker->address,
        'fixo'                    => $faker->phoneNumber,
        'cel'                     => $faker->phoneNumber,
        'complemento'             => $faker->numberBetween(0,50),
        'bairro'                  => $faker->city,
        'cidade'                  => $faker->city,
        'uf'                      => "SP",
        'observacao'              => $faker->text(200),
        'user_id_created'         => 1,
        'user_id_updated'         => 1
    ];
});
